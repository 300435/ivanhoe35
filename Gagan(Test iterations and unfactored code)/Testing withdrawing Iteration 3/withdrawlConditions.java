package withDrawTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import logicteststart.StartTest;

import org.apache.log4j.Level; 
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import static org.junit.Assert.assertThat;



public class withdrawlConditions {
	private static final Logger log = Logger.getLogger(StartTest.class.getName());
	testWithdrawlCode wDraw;
	
	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: withdrawlConditions");

	}

	/**
	 * This is processed/initialized before each test is conducted
	 * 
	 * @throws IOException
	 */
	@Before
	public void setUp() throws IOException {
		System.out.println("@Before(): withdrawlConditions");
		wDraw= new testWithdrawlCode();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): withdrawlConditions");
		wDraw=null;
		
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: withdrawlConditions");
	}

	//test if the player has withdrawn and all his display cards have been discarded
	@Test
	public void testRegularWithdrawing() {
		log.info("\n");
		log.info("@: testRegularWithdrawing");
		int playerPositionNum=1;
		
		
			if(wDraw.gethasPlayerWithdrawn(playerPositionNum)==false){
				log.info("FAILED: PLAYER HAS NOT WITHDRAWN SUCCESSFULLY");
				fail();
			}
			
			if(wDraw.getNumCardsInPlay(playerPositionNum)>0){
				log.info("FAILED: PLAYER HAS NOT WITHDRAWN SUCCESSFULLY");
				fail();
			}
			
			log.info("PASSED: PLAYER HAS WITHDRAWN SUCCESSFULLY");

	}
	
	@Test
	public void  specialWithdrawal(){
		log.info("\n");
		log.info("@: specialWithdrawal");
		
		int position=1;
		
		if(wDraw.gethasPlayerWithdrawn(position)==false){
			log.info("FAILED: PLAYER HAS NOT WITHDRAWN SUCCESSFULLY");
			fail();
		}
		
		if(wDraw.getActivePlayerPosition()==position+1){
			log.info("FAILED: CORRECT PLAYER IS NOT CONTINUING");
			fail();
		}
		
		if(wDraw.displayContainsMaiden(position)==true){
			if( wDraw.ReducedUniqueTokenNum(position)==false){
				log.info("FAILED: CORRECT PLAYER IS NOT CONTINUING");
				fail();
			}
		}
		
		log.info("PASSED: PLAYER HAS WITHDRAWN SUCCESSFULLY AND CORRECT PLAYER IS CONTINUING");
		
	}
	
	
	
}
