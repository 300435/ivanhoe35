package playingCardsTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import logicteststart.StartTest;

import org.apache.log4j.Level; 
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import static org.junit.Assert.assertThat;

public class CardsPlayedCode {
	private static final Logger logg = Logger.getLogger(StartTest.class.getName());

	
	
	public int getActivePlayerPosition() {
		logg.info("At:getActivePlayerPosition" );
		return 0;
	}

	public boolean isStartingTurn() {
		logg.info("At:isStartingTurn" );
		return true;
	}

	public boolean HandContainsColorCards(int playerPosition) {
		logg.info("At:HandContainsColorCards" );
		return false;
	}

	public boolean HandContainsSupporterCards(int playerPosition) {
		logg.info("At:HandContainsSupporterCards" );
		return false;
	}

	public boolean StagedCardsValidity() {
		logg.info("At:StagedCardsValidity" );
		return false;
	}
	
	
}
