package logicteststart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Level; 
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import static org.junit.Assert.assertThat;

@SuppressWarnings("all")
public class StartTest {
	
	private static final Logger logg = Logger.getLogger(StartTest.class.getName());
	StartingCheck check;
	
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: Starting conditions test");
	}
	
	@Before
	public void setUp() throws IOException {
		System.out.println("@Before() Testing");
		logg.info("\n");
		logg.info("Starting testing in: " + this.getClass().getName());
		logg.setLevel(Level.ALL);
	    check= new StartingCheck();
	}
	
	@After
	public void tearDown() {
		System.out.println("@After() testing");
		logg.info("Finishing testing in: "+ this.getClass().getName());
		check=null;
	}
	
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: Finished testing");
	}
	
	
	@Test
	public void testStartingTournamentColor() {
		logg.info("At: testStartingTournamentColor");
	
		String startingColor=check.getStartingColor();
		if(startingColor.isEmpty()){
			logg.info("FAILED: NO COLOR WAS CHOSEN");
			fail();
		}
		
		if(check.getStartingColor().equalsIgnoreCase("purple")){
			if(check.getLastTournamentColor()=="purple"){
				logg.info("FAILED:PURPLE CANNOT BE CHOSEN CONSECUTIVELY");
				fail();
			}
		}

		logg.info("PASSED: COLOR CHOSEN WAS ACCEPTED");
		
	}
	
	@Test
	public void testTournamentStartingplayer(){
		logg.info("At: testTournamentStartingplayer");
		
		if(check.getTournamentNumber()==0){
			logg.info("FAILED: NO TOURNAMENT WAS STARTED");
			fail();
		}
		
		if(check.getTournamentNumber()==1){
			if((check.getpurpleTokenDrawnPostion()+1)!=check.getStartingPlayerPosition()){
				logg.info("FAILED: CORRECT PLAYER IS NOT STARTING THE TOURNAMENT");
				fail();
			}
		}
		
		if(check.getTournamentNumber()>1){
			if(check.getLastTournamentWinnerPosition()!=check.getStartingPlayerPosition()){
				logg.info("FAILED: CORRECT PLAYER IS NOT STARTING THE TOURNAMENT");
				fail();
			}
		}
		
		logg.info("PASSED: CORRECT PLAYER HAS STARTED THE TOURNAMENT");
	}
	
	

}
