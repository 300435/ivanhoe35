package logicteststart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import static org.junit.Assert.assertThat;

public class StartingCheck {
	
	private static final Logger loggCheck = Logger.getLogger(StartingCheck.class.getName());
	
	
	public String getLastTournamentColor(){
		loggCheck.info("At: getLastTournamentColor()");
		String lastColor="purple";
		return lastColor;
			
	}

	public String getStartingColor(){
		loggCheck.info("At: getStartingColor()");
		String startingColor="red";
		return startingColor;
			
	}
	
	public int getTournamentNumber(){
		loggCheck.info("At: getTournamentNumber()");
		return 1;
	}
	
	public int getpurpleTokenDrawnPostion(){
		loggCheck.info("At: getpurpleTokenDrawnPostion()");
		return 0;
	}
	
    public int getStartingPlayerPosition(){
    	loggCheck.info("At: getStartingPlayerPosition()");
		return 1;
	}
    
    public int getLastTournamentWinnerPosition(){
    	loggCheck.info("At: getLastTournamentWinnerPosition()");
    	return 1;
    }
	
	
}
