package actionCardsTest;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


@SuppressWarnings("all")
public class ActionCardsTest {
	
	private static final Logger log = Logger.getLogger(ActionCardsTest.class.getName());
	ActionCardsCode action;

	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: Ending conditions test");
		
	}

	@Before
	public void setUp() throws IOException {
		System.out.println("@Before() Testing");
		log.info("\n");
		log.info("Starting testing in: " + this.getClass().getName());
		 action = new ActionCardsCode();

	}
	
	@After
	public void tearDown() {
		System.out.println("@After() testing");
		log.info("Finishing testing in: " + this.getClass().getName());
		action = null;
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: Finished testing");
	}
	
	@Test
	public void actionCardsPlayed(){
		log.info("@: actionCardsPlayed");
		int playerNum=action.getActivePlayer();
		int targetedPlayer=action.getTargetedPlayer();
		int lastDisplaySize=action.getLastDisplaySize();//saves the size of the display of the player affected by an action card before the affect  
		int currentDisplaySize=action.getCurrentDisplaySize(targetedPlayer);
		
		
		//should be at least 1 after removal
		boolean displayCardAmount= action.getDisplayCardAmount(playerNum);
		//boolean displayCardAmountOpponent= action.getDis
		boolean displayCardAmountAll= action.getDisplayCardAmountAll();
		
	if(action.commitedCardContainsActionCard(playerNum)==true){
			
		if(action.getActionCardFromCommit(playerNum)=="retreat"){
			if(displayCardAmount==false){
				log.info("FAILED: CANNOT TAKE THE LAST CARD FROM DISPLAY");
				fail();
			}
			
			if(action.handContains(action.getActionCardFromCommit(playerNum), playerNum)==false){
				log.info("FAILED: CARD WAS NOT RETURNED BACK TO THE PLAYER'S HAND");
				fail();
			}
		}
		
		
		if(action.getActionCardFromCommit(playerNum)=="disgrace"){
			
			if(displayCardAmountAll==false){
				log.info("FAILED: CANNOT TAKE THE LAST CARD FROM DISPLAY");
				fail();
			}
			
			if(action.getDisplayContainsSupporterCards()==false){
				log.info("FAILED: SUPPORTER CARDS FROM PLAYERS WERE NOT REMOVED PROPERLY");
				fail();
			}
			
		}
		
		if(action.getActionCardFromCommit(playerNum)=="dodge"){
			displayCardAmount= action.getDisplayCardAmount(targetedPlayer);
			
			if(displayCardAmount==false){
				log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
				fail();
			}
			
			if(action.getLastDisplaySize()==currentDisplaySize){
				log.info("FAILED: DODGE DID NOT REMOVE THE CARD FROM OPPONENTS DISPLAY PROPERLY");
				fail();
			}
			
		}
		
		if(action.getActionCardFromCommit(playerNum)=="riposte"){
			displayCardAmount= action.getDisplayCardAmount(targetedPlayer);
			
			if(displayCardAmount==false){
				log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
				fail();
			}
			
			if(action.getLastDisplaySize()==currentDisplaySize){
				log.info("FAILED: RIPOSTE DID NOT REMOVE THE CARD FROM OPPONENTS DISPLAY PROPERLY");
				fail();
			}
			
			if(action.cardAddedByAction()==false){
				log.info("FAILED: CORRECT CARD WAS NOT ADDED FROM OPPONENTS DISPLAY TO ACTIVE PLAYER PROPERLY");
				fail();
			}
			
		}
	
	
		
		
		
		
					
	}
		
	log.info("PASSED: THE CARD WAS PLAYED PROPERLY");
		
		
	}
	

	
}
