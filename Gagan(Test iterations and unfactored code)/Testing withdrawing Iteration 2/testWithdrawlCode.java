package withDrawTest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import logicteststart.StartTest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import static org.junit.Assert.assertThat;

public class testWithdrawlCode {

	private static final Logger logg = Logger.getLogger(StartTest.class.getName());
	
	public boolean gethasPlayerWithdrawn(int playerPosition){
		logg.info("At:gethasPlayerWithdrawn" );
		return true;
	}
	
	
	public int getNumCardsInPlay(int playerPosition){
		logg.info("At: getNumCardsInPlay" );
		return 0;
	}
	
	public int getActivePlayerPosition(){
		logg.info("At: getActivePlayerPosition" );
		return 0;
	}
	
	public boolean displayContainsMaiden(int position){
		logg.info("At: displayContainsMaiden" );
		return true;
	}
	
	public boolean ReducedUniqueTokenNum(int position){
		logg.info("At: ReducedUniqueTokenNum" );
		return false;
	}
	
}
