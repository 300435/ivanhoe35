package playingCardsTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import logicteststart.StartTest;

import org.apache.log4j.Level; 
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import static org.junit.Assert.assertThat;

public class CardsPlayedCode {
	private static final Logger logg = Logger.getLogger(StartTest.class.getName());

	
	
	public int getActivePlayerPosition() {
		logg.info("At:getActivePlayerPosition" );
		return 0;
	}

	public boolean isStartingTurn() {
		logg.info("At:isStartingTurn" );
		return false;
	}

	public boolean HandContainsColorCards(int playerPosition) {
		logg.info("At:HandContainsColorCards" );
		return true;
	}

	public boolean HandContainsSupporterCards(int playerPosition) {
		logg.info("At:HandContainsSupporterCards" );
		return true;
	}

	public boolean StagedCardsValidity() {
		//check if first card staged is a color card, if that color card matches current tournament color,
		//and if other color cards staged are of the tournament color as well. staged cards are automatically valid if they contain only supporter
		//cards(unless its first turn in a tournament, in which case the player has to stage a color card first)
		//staged cards are checked as they are added( can be checked when none are staged, in which case automatically valid
		logg.info("At:StagedCardsValidity" );
		return true;
	}
	
	
}
