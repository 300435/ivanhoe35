package playingCardsTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import logicteststart.StartTest;

import org.apache.log4j.Level; 
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import server.AppServer;
import test.TestServer;
import utils.Config;
import withDrawTest.testWithdrawlCode;
import static org.junit.Assert.assertThat;

public class CardsPlayedTest {
	
	private static final Logger log = Logger.getLogger(StartTest.class.getName());
	CardsPlayedCode cards;
	
	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: CardsPlayedTest");

	}

	/**
	 * This is processed/initialized before each test is conducted
	 * 
	 * @throws IOException
	 */
	@Before
	public void setUp() throws IOException {
		System.out.println("@Before(): CardsPlayedTest");
		cards= new CardsPlayedCode();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): CardsPlayedTest");
		cards=null;
		
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: CardsPlayedTest");
	}


	@Test
	public void testAllowedPlayForCards(){
		log.info("\n");
		log.info("@: testMissingCards" );
		
		
		int playerPosition=cards.getActivePlayerPosition();
		
		if(cards.isStartingTurn()==true){
			
			
			if((cards.HandContainsColorCards(playerPosition)==false) && (cards.HandContainsSupporterCards(playerPosition)==false)){
				log.info("FAILED: STARTING PLAYER(FIRST TURN) CANNOT START, PASSED TO NEXT PLAYER" );
				fail();
			}
			
			if((cards.HandContainsColorCards(playerPosition)==true) && (cards.HandContainsSupporterCards(playerPosition)==true)){
				  if(cards.StagedCardsValidity()==false){ 
					  log.info("FAILED: STARTING PLAYER DID NOT PLAY THE REQUIRED SEQUENCE AND TYPE OF CARDS" );
					  fail();
					  }
				
			}
			
			if((cards.HandContainsColorCards(playerPosition)==false) && (cards.HandContainsSupporterCards(playerPosition)==true)){
				  if(cards.StagedCardsValidity()==false){ 
					  log.info("FAILED: STARTING PLAYER DID NOT PLAY THE REQUIRED SEQUENCE AND TYPE OF CARDS" );
					  fail();
					  }
				
			}
			
		}
		
		if(cards.isStartingTurn()!=true){
			
			if((cards.HandContainsColorCards(playerPosition)==true) || (cards.HandContainsSupporterCards(playerPosition)==true)){
				if(cards.StagedCardsValidity()==false){ 
					  log.info("FAILED: STARTING PLAYER DID NOT PLAY THE REQUIRED SEQUENCE AND TYPE OF CARDS" );
					  fail();
					  }
			}
		
			
		}
		
		log.info("PASSED: THE PLAYER HAS PLAYED VALID CARDS SO FAR");
		
		
		
	}
	
	
	
}
