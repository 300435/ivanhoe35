package actionCardsTest;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class ActionCardsCode {
	private static final Logger log2 = Logger.getLogger(ActionCardsTest.class.getName());
  
	public boolean getDisplayCardAmountAll() {
		//display card amount minus the cards that will be removed 
		//cannot be less than 1(return false if less)
		log2.info("@:getDisplayCardAmountALL");
		return true;
	}

	public boolean getDisplayCardAmount(int playerNum) {
		log2.info("@:getDisplayCardAmount");
		//display card amount minus the cards that will be removed 
				//cannot be less than 1(return false if less)
		return true;
	}

	public int getActivePlayer() {
		log2.info("@:getDisplayCardAmount");
		return 0;
	}

	public boolean commitedCardContainsActionCard(int playerNum) {
		log2.info("@: commitedCardContainsActionCard");
		return true;
	}

	public String getActionCardFromCommit(int playerNum) {
		log2.info("@: getActionCardFromCommit");
		String card="counter-charge";
		return card;
	}

	public boolean handContains(String actionCardFromCommit, int playerNum) {
		log2.info("@: handContains");
		
		return true;
	}

	public boolean getDisplayContainsSupporterCards() {
		log2.info("@: getDisplayContainsSupporterCards");
		//if any of the players have a display of 2 or more and still contain supporter cards, then return false. 
		//the idea is that if players had any other cards with their supporter cards, then their display should not 
		//have any supporter cards left, but if the players had ONLY supporter cards in their display, then the earliest played
		//supporter card will remain in display as per game rules( but this method will still return true). 
		return true;
	}

	public int getTargetedPlayer() {
		log2.info("@: getTargetedPlayer");
		return 0;
	}

	public int getLastDisplaySize(int targetedPlayer) {
		log2.info("@: getLastDisplaySize");
		return 2;
	}

	public int getCurrentDisplaySize(int targetedPlayer) {
		log2.info("@: getCurrentDisplaySize");
		return 1;
	}

	public boolean cardAddedByAction(int activePlayer) {
		//this method will basically check whether the card that was removed from an opponents display
		// was added to the active players display. 
		log2.info("@: cardAddedByAction");
		return true;
	}

	public boolean getLastDisplaySizeOpponenets(int activePlayer) {
		// checks through the last display size of all opponenents of active player and 
		//compares them with current display sizes. if current display sizes are smaller than last display sizes 
		//than the outmaneuver card has removed the cards from opponents displays
		log2.info("@: getLastDisplaySizeAll");
		
		return true;
	}
	
	public boolean getLastDisplaySizeAll() {
		// checks through the last display size of all players and 
		//compares them with their current display sizes. if current display size is smaller than 
		//the last display sizes than the outmaneuver card has removed the cards from all players displays
		log2.info("@: getLastDisplaySizeAll");
		
		return true;
	}

	public int getHighestCardValue() {
		log2.info("@: getHighestCardValue");
		return 0;
	}

	public boolean cardsRemovedByAction(int highestValueCard) {
		//this method will use an array of card serials and check if all the cards removed from all players have the same 
		//value as the highestvaluecard throughout all displays, if so the method returns true, otherwise false
		log2.info("@: cardsRemovedByAction");
		return false;
	}



	
	
}
