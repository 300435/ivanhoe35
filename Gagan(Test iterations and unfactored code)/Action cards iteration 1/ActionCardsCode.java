package actionCardsTest;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class ActionCardsCode {
	private static final Logger log2 = Logger.getLogger(ActionCardsTest.class.getName());
  
	public boolean getDisplayCardAmountAll() {
		//display card amount minus the cards that will be removed 
		//should cannot be less than 1(return false if less)
		log2.info("@:getDisplayCardAmountALL");
		return false;
	}

	public boolean getDisplayCardAmount(int playerNum) {
		log2.info("@:getDisplayCardAmount");
		return false;
	}

	public int getActivePlayer() {
		log2.info("@:getDisplayCardAmount");
		return 0;
	}

	public boolean commitedCardContainsActionCard(int playerNum) {
		log2.info("@: commitedCardContainsActionCard");
		return true;
	}

	public String getActionCardFromCommit(int playerNum) {
		String card="retreat";
		return card;
	}
	
	
}
