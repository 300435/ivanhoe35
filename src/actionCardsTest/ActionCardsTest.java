package actionCardsTest;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import gameLogic.GameData;

@SuppressWarnings("all")
public class ActionCardsTest {

	private static final Logger log = Logger.getLogger(ActionCardsTest.class.getName());

	GameData data;

	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: Ending conditions test");

	}

	@Before
	public void setUp() throws IOException {
		System.out.println("@Before() Testing");
		log.info("\n");
		log.info("Starting testing in: " + this.getClass().getName());

		data = new GameData();
	}

	@After
	public void tearDown() {
		System.out.println("@After() testing");
		log.info("Finishing testing in: " + this.getClass().getName());

		data = null;
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: Finished testing");
	}

	@Test
	public void actionCardsPlayed() {
		log.info("@: actionCardsPlayed");
		int playerNum = data.getActivePlayerPosition();
		int targetedPlayer = data.getTargetedPlayer();
		int lastDisplaySize = data.getLastDisplaySize(targetedPlayer);// saves
																		// the
																		// size
																		// of
																		// the
																		// display
																		// of
																		// the
																		// player
																		// affected
																		// by an
																		// action
																		// card
																		// before
																		// the
																		// affect
		int currentDisplaySize = data.getCurrentDisplaySize(targetedPlayer);

		// should be at least 1 after removal
		boolean displayCardAmount = data.getDisplayCardAmount(playerNum);
		boolean displayCardAmountAll = data.getDisplayCardAmountAll();

		if (data.commitedCardsContainsActionCard(playerNum) == true) {

			if (data.getActionCardFromCommit(playerNum) == "retreat") {
				if (displayCardAmount == false) {
					log.info("FAILED: CANNOT TAKE THE LAST CARD FROM DISPLAY");
					fail();
				}
				int selectedIndex = data.getSelectedIndexForActionCard(playerNum);

				if (data.handContains(selectedIndex, playerNum) == false) {
					log.info("FAILED: CARD WAS NOT RETURNED BACK TO THE PLAYER'S HAND");
					fail();
				}
			}

			if (data.getActionCardFromCommit(playerNum) == "disgrace") {

				if (displayCardAmountAll == false) {
					log.info("FAILED: CANNOT TAKE THE LAST CARD FROM DISPLAY");
					fail();
				}

				if (data.getDisplayContainsSupporterCards() == false) {
					log.info("FAILED: SUPPORTER CARDS FROM PLAYERS WERE NOT REMOVED PROPERLY");
					fail();
				}

			}

			if (data.getActionCardFromCommit(playerNum) == "dodge") {
				displayCardAmount = data.getDisplayCardAmount(targetedPlayer);

				if (displayCardAmount == false) {
					log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
					fail();
				}

				if (data.getLastDisplaySize(targetedPlayer) == currentDisplaySize) {
					log.info("FAILED: DODGE DID NOT REMOVE THE CARD FROM OPPONENTS DISPLAY PROPERLY");
					fail();
				}

			}

			if (data.getActionCardFromCommit(playerNum) == "riposte") {
				displayCardAmount = data.getDisplayCardAmount(targetedPlayer);

				if (displayCardAmount == false) {
					log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
					fail();
				}

				if (data.getLastDisplaySize(targetedPlayer) == currentDisplaySize) {
					log.info("FAILED: RIPOSTE DID NOT REMOVE THE CARD FROM OPPONENTS DISPLAY PROPERLY");
					fail();
				}

				if (data.cardAddedByAction(playerNum) == false) {
					log.info("FAILED: CORRECT CARD WAS NOT ADDED FROM OPPONENTS DISPLAY TO ACTIVE PLAYER PROPERLY");
					fail();
				}

			}

			if (data.getActionCardFromCommit(playerNum) == "outmaneuver") {

				if (displayCardAmountAll == false) {
					log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
					fail();
				}

				if (data.getLastDisplaySizeOpponenets(playerNum) == false) {
					log.info("FAILED: OUTMANEUVER DID NOT REMOVE THE CARD FROM ALL OPPONENTS DISPLAY PROPERLY");
					fail();
				}

			}

			if (data.getActionCardFromCommit(playerNum) == "counter-charge") {

				if (displayCardAmountAll == false) {
					log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
					fail();
				}

				if (data.getLastDisplaySizeAll() == false) {
					log.info("FAILED: COUNTER-CHARGE DID NOT REMOVE THE REQUIRED CARDS FROM ALL DISPLAYS PROPERLY");
					fail();
				}
				int highestCardValue = data.getHighestCardValue();
				if (data.cardsRemovedByAction(highestCardValue) == false) {
					log.info(
							"FAILED: COUNTER-CHARGE DID NOT REMOVE THE REQUIRED VALUE CARDS FROM ALL DISPLAYS PROPERLY");
					fail();
				}

			}

			if (data.getActionCardFromCommit(playerNum) == "charge") {

				if (displayCardAmountAll == false) {
					log.info("FAILED: CANNOT DISCARD THE LAST CARD FROM OPPONENTS DISPLAY");
					fail();
				}

				if (data.getLastDisplaySizeAll() == false) {
					log.info("FAILED: CHARGE DID NOT REMOVE THE REQUIRED CARDS FROM ALL DISPLAYS PROPERLY");
					fail();
				}
				int lowestCardValue = data.getLowestCardValue();
				if (data.cardsRemovedByAction(lowestCardValue) == false) {
					log.info("FAILED: CHARGE DID NOT REMOVE THE REQUIRED VALUE CARDS FROM ALL DISPLAYS PROPERLY");
					fail();
				}

			}

		}

		log.info("PASSED: THE CARD WAS PLAYED PROPERLY");

	}

}
