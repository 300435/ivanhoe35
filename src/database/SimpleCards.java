package database;


//This class includes color cards and supporter cards
public class SimpleCards extends Cards {
	
//	private int value;
//	private String color;
//	private String name;
	
//	public SimpleCards()
//	{
//		this.value = 0;
//		this.color = "";
//		this.sNum = 0;
//	}
	
	public SimpleCards(int v, int snum, String c)
	{
		this.value = v;
		this.color = c;
		this.setsNum(snum);
		setDescription(v, c);
		setName(v, c);
		
	}
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
	
	private void setDescription(int v, String c)
	{
		this.description = "This card is " + c + " " + v; 
	}
	
	private void setName(int value, String color) {
		this.name = color + " " + value;
	}
	
	
}