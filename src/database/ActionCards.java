package database;

@SuppressWarnings("all")
public class ActionCards extends Cards
{
	//private String name;
	private boolean affectsSelf = false;
	private boolean affectsOneOpp = false;
	private boolean affectsAll = false;
	private String actOnWhichCard = "any";
	private int playOnPos = 0;
	
	public ActionCards(String name)
	{
		this.name = name;
		switch(name)
		{
			case "dodge":			this.affectsOneOpp = true;
									this.setsNum(700);
									this.description = "Dodge: You may discard any one card from any one opponent's display.";
									break;
						
			case "disgrace":		this.affectsAll = true;
									this.setsNum(701);
									this.description = "Disgrace: All players must discard all their supporter cards from display.";
									this.actOnWhichCard = "supporter";
									break;
								
			case "retreat":			this.affectsSelf = true;
									this.setsNum(702);
									this.description = "Retreat: You may take any one card from your own display back to your hand.";
									break;
								
			case "riposte":			this.affectsOneOpp = true;
									this.affectsSelf = true;
									this.setsNum(703);
									this.description = "Riposte: You may take the last card of any one opponent's display and add it to your own display.";
									break;
								
			case "outmaneuver":		this.affectsAll = true;
									this.setsNum(706);
									this.description = "Outmaneuver: All your opponents must discard their last card from their displays.";
									break;
								
			case "counter-charge":	this.affectsAll = true;
									this.setsNum(707);
									this.description = "Counter-charge: Identify the highest value card throughout all displays."
											+ " All players must remove all cards of this value from their display.";
									break;
									
			case "charge":			this.actOnWhichCard = "simple";
									this.setsNum(708);
									this.affectsAll = true;
									this.description = "Charge: Identify the lowest value card throughout all displays. "
											+ "All players must remove all cards of this value from their display.";
									break;						
								
											
			default:				//System.out.println("card name invalid");
									this.name = "";
									break;
		}		
	}
	
}


//List of the action cards to be implemented in iteration 2
//-------------------------------------------------------------------------
//case "break lance":		this.actOnWhichCard = "purple";
//this.affectsOneOpp = true;
//this.setsNum(709);
//this.description = "Break Lance: One opponent must discard all purple cards from his display.";
//break;

//case "adapt":			this.affectsAll = true;
//this.setsNum(710);
//this.description = "Adapt: Each player may only keep one card of each value. "
//		+ "All other cards with the same value are discarded. "
//		+ "Each player must decide which cards he discards.";
//break;
//
//case "drop weapon":		this.affectsAll = true;
//this.setsNum(711);
//this.description = "Drop Weapon: Change the tournament color from red, blue or yellow to green.";
//break;
//
//case "change weapon":	this.affectsAll = true;
//this.setsNum(712);
//this.description = "Change Weapon: Change the tournament color from red, blue or yellow to a different one of these colors.";
//break;
//
//case "unhorse":			this.affectsAll = true;
//this.setsNum(713);
//this.description = "Unhorse: Change the tournament color from purple to red, blue or yellow.";
//break;
//
//case "knock down":		this.affectsOneOpp = true;
//this.affectsSelf = true;
//this.setsNum(714);
//this.description = "Knock Down: You may draw at random one card from any one opponent's hand and add it to yours. "
//		+ "(without revealing the card)";
//break;
//
//case "outwit":			this.affectsOneOpp = true;
//this.affectsSelf = true;
//this.setsNum(716);
//this.description = "Outwit: Exchange one card from your display with one card from one of the opponent's display. "
//		+ "This may include the SHIELD and STUNNED action cards";
//break;
//
//case "shield":			this.affectsSelf = true;
//this.setsNum(717);
//this.description = "Shield: Place this card face up in front of you, but separate from your display."
//		+ "As long as you have this card in front of you, no other action card can affect you.";
//break;
//
//case "stunned":			this.affectsOneOpp = true;
//this.setsNum(718);
//this.description = "Stunned: You may place this card face up in front of any one opponent still in the tournament, "
//		+ "but separate from their display. "
//		+ "As long as a player is STUNNED, he can only add one new card to his display each turn.";
//break;
//
//case "ivanhoe":			this.affectsAll = true;
//this.setsNum(719);
//this.actOnWhichCard = "action";
//this.description = "Ivanhoe: You may play this card at any time as long as you are in the tournament. "
//		+ "Use the IVANHOE card to cancel all affects of any one action card just played.";
//break;
//
