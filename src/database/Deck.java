package database;

import java.util.Collections;
import java.util.LinkedList;


import utils.RawData;

public class Deck {

	private final LinkedList<Cards> allCards = new LinkedList<Cards>();
	private LinkedList<Cards> deck = new LinkedList<Cards>();
	//getter for deck
	public LinkedList<Cards> getDeck() {
		return deck;
	}

	private LinkedList<Cards> discarded = new LinkedList<Cards>();
	RawData rawData;

	/**
	 * 
	 */
	public Deck() {
		super();

		rawData = new RawData();
		// Initializing purple cards
		initializeSimple(3, "purple", 4, 0);
		initializeSimple(4, "purple", 4, 4);
		initializeSimple(5, "purple", 4, 8);
		initializeSimple(7, "purple", 2, 12);

		// Initializing red cards
		initializeSimple(3, "red", 6, 100);
		initializeSimple(4, "red", 6, 106);
		initializeSimple(5, "red", 2, 112);

		// Initializing blue cards
		initializeSimple(2, "blue", 4, 200);
		initializeSimple(3, "blue", 4, 204);
		initializeSimple(4, "blue", 4, 208);
		initializeSimple(5, "blue", 2, 212);

		// Initializing yellow cards
		initializeSimple(2, "yellow", 4, 300);
		initializeSimple(3, "yellow", 8, 304);
		initializeSimple(4, "yellow", 2, 312);

		// Initializing green cards
		initializeSimple(1, "green", 14, 400);

		// ------------------------------------

		// initializing maiden cards
		initializeSimple(6, "maiden", 4, 500);

		// initializing squire cards
		initializeSimple(2, "squire", 8, 504);
		initializeSimple(3, "squire", 8, 512);

		// -------------------------------------

		// initializing action cards
		initializeAction(rawData.actionCards);

		// Shuffling the deck
		Collections.shuffle(deck);
	}
	public void initializeSimple(int value, String color, int quantity, int sNum) {
		Cards sc = new SimpleCards(value, sNum, color);
		for (int i = 0; i < quantity; i++) {
			allCards.add(sc);
			deck.add(sc);
		}
	}

	public void initializeAction(String[] actionNames) {
		for (String name : actionNames) {
			ActionCards ac = new ActionCards(name);
			allCards.add(ac);
			deck.add(ac);
			if(name == "riposte") {
				allCards.add(ac);
				deck.add(ac);
				allCards.add(ac);
				deck.add(ac);
			}
		}
	}

	public void initializeHand(Player p) {
		for (int i = 0; i < 8; i++) {
			p.addToHand(deck.get(i));
			deck.remove(i);
		}
	}

	public void drawOneCard(Player p) {
		discardedToDeck();
		Cards newsc = this.deck.pop();
		p.addToHand(newsc);
	}

	public void discard(int snum) {
		for (Cards card : allCards)
			if (card.getsNum() == snum) {
				discarded.add(card);
				break;
			}
	}

	public void discard(Cards card) {
		discarded.add(card);
	}

	public void discardedToDeck() {
		if (this.deck.size() == 0) {
			for (int i = 0; i < this.discarded.size(); i++)
				this.deck.add(this.discarded.remove(i));
			Collections.shuffle(this.deck);
		}
	}

	public void deal(Player[] players) {
		for (Player p : players) {
			initializeHand(p);
		}

	}
	
	//makes sure that each player's display was cleared out and added to discard pile
		public boolean refreshAllDisplays(Player[] players) {
			int counter = 0;
			for(Player p: players) {
				for(int i =0; i < p.getDisplaySize(); i++)
					discard(p.removeCardFromDisplay(i));
				if(p.getDisplaySize() == 0)
					counter++;
			}
			
			if(players.length == counter)
				return true;
			else
				return false;
		}
	
	
}
