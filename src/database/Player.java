package database;

import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("all")
public class Player {

	private int numCardsInPlay;// the number of cards in display
	// changed as cards are updated on a players display one by one but if an
	// action card comes up when going through the card list
	// then a test could be run based on the last display size if
	// the current display size is larger or smaller or whatever is required for
	// tests.
	private int lastDisplaySize;
	private ArrayList<Cards> display;
	private ArrayList<Cards> hand;
	private ArrayList<String> tokens;

	private boolean isWithdrawn;
	private boolean myTurn;

	private int playerTotal;
	private int myTargetPlayer;
	private int myTargetCard;
	private boolean winTourny = false;
	private boolean winGame = false;

	public ArrayList<Integer> onStage;
	Random random = new Random();

	// some things do and dont need refresing each tournament--inheritance?
	public Player() {
		super();
		this.hand = new ArrayList<Cards>();
		this.display = new ArrayList<Cards>();
		this.tokens = new ArrayList<String>();
		this.isWithdrawn = false;
		this.myTurn = false;

		this.myTargetPlayer = -1;
		this.myTargetCard = -1;
		this.playerTotal = 0;

		this.onStage = new ArrayList<Integer>();

	}

	// this function creates a shallow list of playable numbers while enforcing
	// rules
	// and maintaining order

	public boolean stage(int serialNumber) {

		// if calling player tries to stage while withrawn or out-of-turn
		// if (isWithdrawn || !myTurn) {
		// return false;
		// }

		/* Moving to commit() when i write it */
		// if first card attempted is not of a colour
		// if (onStage.length == 0 && (serialNumber / 100) > 5)
		// return false;

		// if cards aren't of one colour

		if (onStage.size() > 1) {
			int colour;
			// extract colour from serial
			colour = onStage.get(0) / 100;
			for (int i = 1; i < onStage.size(); i++) {
				int serialColour = onStage.get(i) / 100;
				if (serialColour == 5 || serialColour == 7)
					continue;
				if (serialColour != colour)
					return false;
			}
			
			for(int i : onStage)
				if(serialNumber >= 700 && i>=700)
					return false;
		}
		onStage.add(serialNumber);
		return true;

	}

	// TODO any more Turn/Tournament based impacts for player?
	public void setNewTournamentConditions() {
		this.lastDisplaySize = 0;
		this.numCardsInPlay = 0;
		this.isWithdrawn = false;
		this.playerTotal = 0;
		this.myTargetCard = 0;
		this.myTargetPlayer = 0;
		this.onStage.clear();
		this.myTurn = false;

	}
	
	public void wonTournament(String token, int numPlayers) {
		this.myTurn = true;
		if(!this.tokens.contains(token)) {
			this.tokens.add(token);
		}
		this.wonGame(numPlayers);
	}
	
	public void wonGame(int numPlayers) {
		if(numPlayers <= 3)
			if(this.tokens.size() == 5)
				this.winGame = this.winTourny = true;
		if(numPlayers > 3)
			if(this.tokens.size() == 4)
				this.winGame = this.winTourny = true;
		
		if(this.winGame == false)
			this.winTourny = true;
		
	}

	public void setNewTurnConditions() {
		this.lastDisplaySize = getDisplaySize();
		// this.onStage = new int[getHandSize()];
		this.onStage.clear();
	}

	public int getPlayerTotal() {
		return playerTotal;
	}

	public void setPlayerTotal(int playerTotal) {
		this.playerTotal = playerTotal;
	}

	public void printHand() {
		for (Cards card : hand) {
			System.out.println(card.getDescription());
		}
	}

	public void addToHand(Cards card) {
		hand.add(card);
	}

	@Override
	public String toString() {
		return "Player [display=" + display + ", hand=" + hand + ", tokens=" + tokens + ", isWithdrawn=" + isWithdrawn
				+ ", myTurn=" + myTurn + ", playerTotal=" + playerTotal + "]";
	}

	// move was validated and applied by RulesEngine
	// now move the cards
	public void commit() {
		for (int serial : onStage) {
			for (Cards card : hand){
				if(card.getsNum() == serial && serial >=700)
					hand.remove(card);
				if(card.getsNum() == serial) {
					display.add(card);
					hand.remove(card);
				}
			}
		}
		onStage = null;
	}

	public Cards removeCardFromDisplay(int index) {
		Cards card = display.get(index);
		display.remove(index);
		return card;
	}

	public Cards getCardFromDisplay(int index) {
		Cards card = display.get(index);
		return card;
	}

	public void withdraw() {
		isWithdrawn = true;
		int n = 0;
		for(Cards card : display) {
			if(card.getsNum() == 500)
				if(tokens.size() > 0) {
					n = random.nextInt(tokens.size() - 1);
					tokens.remove(n);
				}		
		}
	}

	public boolean isWithdrawn() {
		return isWithdrawn;
	}

	public int getDisplaySize() {
		return display.size();
	}

	public int getHandSize() {
		return hand.size();
	}

	public int getMyTargetPlayer() {
		return myTargetPlayer;
	}

	public void setMyTargetPlayer(int myTargetPlayer) {
		this.myTargetPlayer = myTargetPlayer;
	}

	public int getMyTargetCard() {
		return myTargetCard;
	}

	public void setMyTargetCard(int myTargetCard) {
		this.myTargetCard = myTargetCard;
	}

	public boolean isMyTurn() {
		return myTurn;
	}

	public void setMyTurn(boolean wanted) {
		if (wanted)
			onStage.clear();

		this.myTurn = wanted;

	}

	public int updatePlayerTotal() {
		for (Cards card : display) {
			playerTotal += card.getValue();
		}
		return playerTotal;
	}

	public ArrayList<Cards> getPlayerDisplay() {

		return display;
	}

	public ArrayList<Cards> getPlayerHand() {

		return hand;
	}

	public ArrayList<String> getTokens() {
		return tokens;
	}

	public void addTokens(String tokens) {
		this.tokens.add(tokens);
	}

	public int getTokensSize() {
		return this.tokens.size();
	}

	public boolean containsToken(String token) {
		return this.tokens.contains(token);
	}
	
	public int getLastDisplaySize() {
		return lastDisplaySize;
	}

	public void setLastDisplaySize(int lastDisplaySize) {
		this.lastDisplaySize = lastDisplaySize;
	}

	public boolean getWinTourny() {
		return winTourny;
	}

	public void setWinTourny(boolean winTourny) {
		this.winTourny = winTourny;
	}

	public boolean getWinGame() {
		return winGame;
	}

	public void setWinGame(boolean winGame) {
		this.winGame = winGame;
	}


}