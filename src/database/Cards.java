package database;

public class Cards {

	@Override
	public String toString() {
		return this.getName();
	}

	protected String description;
	protected int value;
	protected String color;
	protected int sNum;
	protected String name;
	boolean onStage;

	public boolean isOnStage() {
		return onStage;
	}

	public void setOnStage(boolean onStage) {
		this.onStage = onStage;
	}

	public int getValue() {
		return this.value;
	}

	public void setValue(int v) {
		this.value = v;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String c) {
		this.color = c;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String d) {
		this.description = d;
	}

	public int getsNum() {
		return sNum;
	}

	public void setsNum(int sNum) {
		this.sNum = sNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
