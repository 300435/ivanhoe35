package network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import gui.guiPlayer;
import utils.Logit;

public class ClientThread extends Thread {
	private Socket socket = null;
	private GameClient client = null;
	private guiPlayer gooey = null;
	private BufferedReader streamIn = null;
	private BufferedWriter streamOut = null;
	private boolean done = false;

	public ClientThread(GameClient client, Socket socket) {
		this.client = client;
		this.socket = socket;
		this.open();
		this.start();
	}

	public void open() {
		try {
			streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		} catch (IOException ioe) {
			System.out.println("Error getting input stream");
			Logit.getInstance().exception(this, ioe);
			client.stop();
		}
	}

	public void close() {
		done = true;
		try {
			if (streamIn != null)
				streamIn.close();
			if (socket != null)
				socket.close();
			this.socket = null;
			this.streamIn = null;
		} catch (IOException ioe) {
			Logit.getInstance().exception(this, ioe);
		}
	}

	public void run() {
		System.out.println("Client Thread " + socket.getLocalPort() + " running.");
		while (!done) {
			try {
				client.handle(streamIn.readLine());
			} catch (IOException ioe) {
				Logit.getInstance().exception(this, ioe);
			}
		}
	}

}
