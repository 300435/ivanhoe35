
package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import gameLogic.GameData;
import gameLogic.RulesEngine;
import gui.board;
import gui.guiPlayer;
import utils.Config;
import utils.Logit;

public class GameServer implements Runnable {
	public static final int INITIALIZED = 1;
	public static final int WAITING = 2;
	public static final int READY_TO_BEGIN = 3;

	public static final int START_TOURNAMENT = 5;
	public static final int MOVE_MADE = 6;
	public static final int VALID_MOVE_CONFIRMED = 7;
	public static final int NEXT_MOVE = 8;
	public static final int TOURNAMENT_COMPLETE = 9;
	public static final int GAME_COMPLETE = 10;
	protected static int STATE;

	private static int clientCount = 0;
	private static GameData gameData;
	private RulesEngine rulesEngine;
	public board daBoard;
	private ArrayList<guiPlayer> gooeys = new ArrayList<guiPlayer>();
	private Thread thread = null;
	public ServerSocket server = null;
	private HashMap<Integer, ServerThread> clients;
	Set<Integer> clientKeyset;
	int clientsNumber;

	private Logger logger = Logit.getInstance().getLogger(this);

	public GameServer(int port, int maxPlayers) {
		STATE = INITIALIZED;
		this.clientsNumber = maxPlayers;
		try {

			System.out.println("Binding to port " + port + ", please wait  ...");
			Logit.getInstance().write(this, "Binding to port " + port);
			logger.info("Binding to port " + port);
			clients = new HashMap<Integer, ServerThread>();
			server = new ServerSocket(port);
			server.setReuseAddress(true);
			start();
		} catch (IOException ioe) {
			Logit.getInstance().exception(ioe);
			logger.fatal(ioe);
		}

	}

	/** start Master Thread */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
			Logit.getInstance().write(this, "Server started: " + server, thread.getId());
			logger.info(String.format("Server started: %s %d", server, thread.getId()));
			STATE = WAITING;
		}
	}

	/** The main server thread starts and is listening for clients to connect */
	public void run() {
		gameData = new GameData(clientsNumber, STATE);
		rulesEngine = new RulesEngine(gameData);
		daBoard = new board(gameData);
		while (clientCount < clientsNumber) {
			try {
				if (clientCount < clientsNumber) {
					Logit.getInstance().write(this, "Waiting for a client ...");
					logger.info("Waiting for a client ...");
					addThread(server.accept());
				}
			} catch (IOException e) {
				Logit.getInstance().exception(this, e);
			}
		}
		board.main();
	}

	/**
	 * Client connection is accepted and now we need to handle it and register
	 * it and with the server | HashTable
	 **/
	private void addThread(Socket socket) {
		Logit.getInstance().write(this, "Client accepted", socket);
		if (clientCount < clientsNumber) {
			try {
				/** Create a separate server thread for each client */
				ServerThread serverThread = new ServerThread(this, socket);
				/** Open and start the thread */
				serverThread.open();
				serverThread.start();
				clients.put(serverThread.getID(), serverThread);
				guiPlayer guiP = new guiPlayer(gameData.players[clientCount]);
				System.out.println(gameData.players[clientCount]);
				GameServer.clientCount++;
				gooeys.add(guiP);
				guiP.main();
				if (clientCount == clientsNumber) {
					clientKeyset = clients.keySet();
					STATE = READY_TO_BEGIN;

				}
				System.out.println(this);
			} catch (IOException e) {
				Logit.getInstance().exception(this, e);
			}
		} else {

			// client refused
			logger.info(String.format("Client Tried to connect: %s", socket));
			logger.info(String.format("Client refused: maximum number of clients reached: d", Config.MAX_CLIENTS));

			Logit.getInstance().write(this, "Client Tried to connect", socket);
			Logit.getInstance().write(this, "Client refused: maximum number of clients reached", Config.MAX_CLIENTS);
		}
	}

	int[] recievedSerials;
	Scanner scanner;

	public synchronized void handle(int ID, String string) {
		// STATE = MOVE_MADE;
		// scanner = new Scanner(string);
		// recievedSerials = new int[55];
		// int i = 0;
		// while (scanner.hasNextInt()) {
		// recievedSerials[i++] = scanner.nextInt();
		// }
		//
		// // check for state-series serial 600s
		// for (int serial : recievedSerials) {
		// if (serial / 100 == 6) {
		//
		// if (i % 600 == 66) {
		// gameData.players[ID].withdraw();
		// gameData.checkWin();
		// gameData.nextPlayersTurn();
		// } else if (i % 600 == 76) {
		// gameData.players[ID].win();
		// // TODO gameData.newTournament();
		// } else { // 86
		// gameData.players[ID].winGame();
		// // TODO Broadcast all
		// }
		// }
		// }
		// // submits move to rules engine
		// // where impact is evaluated and affected in rules engine for true
		// value
		// if (rulesEngine.validateMove(recievedSerials, ID, STATE)) {
		// gameData.players[ID].commit();
		// gameData.nextPlayersTurn();
		// STATE = VALID_MOVE_CONFIRMED;
		// }

	}

	@Override
	public String toString() {
		return "GameServer [STATE=" + GameServer.STATE + ", clientPorts=" + clientKeyset + ", client count="
				+ clientCount + "]";
	}

	/** Try and shutdown the client cleanly */
	public synchronized void remove(int ID) {
		if (clients.containsKey(ID)) {
			ServerThread toTerminate = clients.get(ID);
			clients.remove(ID);
			clientCount--;

			toTerminate.close();
			toTerminate = null;
		}
	}

	/** Shutdown the server cleanly */
	public void shutdown() {
		Set<Integer> keys = clients.keySet();

		if (thread != null) {
			thread = null;
		}

		try {
			for (Integer key : keys) {
				clients.get(key).close();
				clients.put(key, null);
			}
			clients.clear();
			server.close();
		} catch (IOException e) {
			Logit.getInstance().exception(this, e);
		}
		logger.info(String.format("Server Shutdown cleanly %s", server));
		Logit.getInstance().write(this, "Server Shutdown cleanly", server);
		Logit.getInstance().close();
	}

	public static int getClientCount() {
		return clientCount;
	}

}
