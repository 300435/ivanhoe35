package network;

import java.util.Scanner;

import utils.Config;
import utils.Level;
import utils.Logit;

public class StartServer {

	private static Boolean done = Boolean.FALSE;
	private static Boolean started = Boolean.FALSE;

	private static Scanner sc = new Scanner(System.in);
	private static GameServer gameServer = null;
	private static int numPlayers;

	public static void main(String[] argv) {

		System.out.println("input a number 2-5 for player count");

		do {
			numPlayers = sc.nextInt();

			if (numPlayers <= Config.MAX_CLIENTS || numPlayers >= Config.MIN_CLIENTS && !started) {
				System.out.println("Starting server ...");
				Logit.getInstance().setLevel(Level.STDOUT);
				// use instructed numPlayers to set maximum Queue size for
				// players
				gameServer = new GameServer(Config.DEFAULT_PORT, numPlayers);
				// gameServer.STATE = GameServer.INITIALIZED;
				started = Boolean.TRUE;
			}

		} while (!done);

		System.exit(1);
	}
}
