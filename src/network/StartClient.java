package network;

import utils.Config;

public class StartClient {
	//private static int numClients;
	public static void main(String[] argv) {
		new GameClient(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
	}
}
