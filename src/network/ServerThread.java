package network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import utils.Logit;

public class ServerThread extends Thread {
	private int ID = -1;
	private Socket socket = null;
	private GameServer server = null;
	private BufferedReader streamIn = null;
	private BufferedWriter streamOut = null;
	// private CharArrayReader streamIn = null;
	// private CharArrayWriter streamOut = null;
	private String clientAddress = null;

	private boolean done = false;

	public ServerThread(GameServer server, Socket socket) {
		super();
		this.server = server;
		this.socket = socket;
		this.ID = socket.getPort();
		this.clientAddress = socket.getInetAddress().getHostAddress();
	}

	public int getID() {
		return this.ID;
	}

	public String getSocketAddress() {
		return clientAddress;
	}

	/**
	 * The server processes the messages and passes it to the client to send it
	 */
	public void send(int[] move) {
		try {
			streamOut.write(move.toString());
			streamOut.flush();
		} catch (IOException ioe) {
			Logit.getInstance().exception(this, ioe, ID);
			server.remove(ID);
		}
	}

	/**
	 * server thread that listens for incoming message from the client on the
	 * assigned port
	 */
	public void run() {
		Logit.getInstance().write(this, "Server Thread Running", ID);
		while (!done) {
			/** Received a message and pass to the server to handle */
			try {
				server.handle(ID, streamIn.readLine());
			} catch (IOException e) {

				e.printStackTrace();
				server.remove(ID);
			} // TODO int array
		}
	}

	public void open() throws IOException {
		streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	}

	public void close() {
		try {
			if (socket != null)
				socket.close();
			if (streamIn != null)
				streamIn.close();

			this.done = true;
			this.socket = null;
			this.streamIn = null;
		} catch (IOException e) {
		}
	}

}
