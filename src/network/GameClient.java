package network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import utils.Config;
import utils.Logit;
//import gui.guiPlayer;
//import gameLogic.GameData;
//import database.Player;

public class GameClient implements Runnable {
	private int ID = 0;
	private Socket socket = null;
	private Thread thread = null;
	private ClientThread client = null;
	private BufferedReader console = null;
	private BufferedReader streamIn = null;
	private BufferedWriter streamOut = null;
//	private Player myPlayer = new Player();
//	private guiPlayer myGUI;
//	private static GameData gameData;
//	private static int numClients = 0;
//	private int myIndex = 0;

	public GameClient(String serverName, int serverPort) {
		System.out.println(ID + ": Establishing connection. Please wait ...");

		try {
			this.socket = new Socket(serverName, serverPort);
			this.ID = socket.getLocalPort();
			System.out.println(ID + ": Connected to server: " + socket.getInetAddress());
			System.out.println(ID + ": Connected to portid: " + socket.getLocalPort());
//			this.myIndex = this.numClients;
//			this.numClients++;
//			myPlayer = gameData.players[myIndex];
//			myGUI = new guiPlayer(myPlayer);
//			myGUI.main();
			this.start();
			
		} catch (UnknownHostException uhe) {
			System.err.println(ID + ": Unknown Host");
			Logit.getInstance().exception(this, uhe);
		} catch (IOException ioe) {
			System.out.println(ID + ": Unexpected exception");
			Logit.getInstance().exception(this, ioe);
		}
	}

	public int getID() {
		return this.ID;
	}

	public void start() throws IOException {
		try {
			console = new BufferedReader(new InputStreamReader(System.in));
			streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

			if (thread == null) {
				client = new ClientThread(this, socket);
				thread = new Thread(this);
				thread.start();
			}
		} catch (IOException ioe) {
			Logit.getInstance().exception(this, ioe);
			throw ioe;
		}
	}

	public void run() {
		System.out.println(ID + ": Client Started...");
		while (thread != null) {
			try {
				if (streamOut != null) {
					streamOut.flush();
					streamOut.write(console.readLine() + "\n");
				} else {
					System.out.println(ID + ": Stream Closed");
				}
			} catch (IOException e) {
				Logit.getInstance().exception(this, e);
				stop();
			}
		}
		System.out.println(ID + ": Client Stopped...");
	}

	public void handle(String msg) {
		if (msg.equalsIgnoreCase("quit!")) {
			System.out.println(ID + "Good bye. Press RETURN to exit ...");
			stop();
		} else {
			System.out.println(msg);
		}
	}

	public void stop() {
		try {
			if (thread != null)
				thread = null;
			if (console != null)
				console.close();
			if (streamIn != null)
				streamIn.close();
			if (streamOut != null)
				streamOut.close();

			if (socket != null)
				socket.close();

			this.socket = null;
			this.console = null;
			this.streamIn = null;
			this.streamOut = null;
		} catch (IOException ioe) {
			Logit.getInstance().exception(this, ioe);
		}
		client.close();
	}

	@SuppressWarnings("unused")
	public static void main(String args[]) {
		GameClient client = new GameClient(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
	}
}
