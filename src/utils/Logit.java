package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import network.ServerThread;

public class Logit {
	private static Logit _instance = null;

	private Level level = Level.OFF;

	private FileWriter clientLogger = null;
	private FileWriter serverLogger = null;

	// private FileWriter testLogger = null;

	public Logger getLogger(Object o) {
		return Logger.getLogger(o.getClass().getName());

	}

	private Logit() {
		String userDir = System.getProperty("user.dir");
		String configFile = String.format("%s\\%s\\%s.properties", userDir, "properties", "log4j");
		PropertyConfigurator.configure(configFile);

		try {
			String clientLogFile = String.format("%s//logs//Client.log", userDir);
			String serverLogFile = String.format("%s//logs//Server.log", userDir);
			// String testLogFile = String.format("%s//logs//Tests.log",
			// userDir);
			/**
			 * First we need to ensure our log directory exists | if not create
			 * it
			 */
			File logDir = new File(String.format("%s//logs", userDir));
			if (!logDir.exists())
				logDir.mkdir();

			clientLogger = new FileWriter(clientLogFile);
			serverLogger = new FileWriter(serverLogFile);
			// testLogger = new FileWriter(testLogFile);

		} catch (IOException e) {
			System.out.printf("Error while opening log file: %s\n", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static Logit getInstance() {
		if (_instance == null) {
			synchronized (Logit.class) {
				_instance = new Logit();
			}
		}
		return _instance;
	}

	// StartServer @ 30
	public void setLevel(Level traceLevel) {
		this.level = traceLevel;
	}

	// called by GameServer @ 48 , 84
	public void write(Object o, String m) {
		try {
			serverLogger.write(format(o, m));
		} catch (IOException e) {
			System.err.printf("Exception %s thrown from %s\n", this, e.getMessage());
		}
		if (level == Level.STDOUT)
			System.out.println(format(o, m));
	}

	public void writeToClientFile(Object o, String m) {
		try {
			clientLogger.write(format(o, m));
		} catch (IOException e) {
			System.err.printf("Exception %s thrown from %s\n", this, e.getMessage());
		}

		if (level == Level.STDOUT)
			System.out.println(format(o, m));
	}

	// ServerThread @ run()
	// GameServer @ 75 , 116 , 123
	public void write(Object o, String m, int i) {
		String message = String.format("%s %d\n", m, i);
		write(o, message);
	}

	public void write(Object o, String m, long l) {
		String message = String.format("%s %s\n", m, Long.toString(l));
		write(o, message);
	}

	// called by GameServer @ 98 , 115
	public void write(Object o, String m, Socket socket) {
		String message = String.format("%s : Client Address : [%15s] Client Socket: [%-6d]\n", m,
				socket.getRemoteSocketAddress(), socket.getPort());
		write(o, message);
	}

	// called by GameServer @ 65
	public void exception(Exception e) {
		String message = String.format("Exception thrown : %s \n", e.getMessage());
		if (Config.PRINT_STACK_TRACE)
			e.printStackTrace();
		write(this, message);
	}

	// ServerThread @ 64-run()
	// GameServer @ 88,109,182
	// GameClient @ 34,37,57,73,108
	// ClientThread @ 28,43,53
	public void exception(Object o, Exception e) {
		String message = String.format("Exception thrown : %s \n", e.getMessage());
		if (Config.PRINT_STACK_TRACE)
			e.printStackTrace();
		write(o, message);
	}

	// ServerThread @ 47
	public void exception(Object o, IOException ioe, int ID) {
		String message = String.format("Exception in client/server thread [%3d] ID [%d]\n",
				o.getClass().getSimpleName(), ioe, ID);
		write(o, message);
	}

	// gamerServer at 185
	public void write(Object o, String m, ServerSocket server) {
		String message = format(m, String.format("%s:%d", server.getInetAddress(), server.getLocalPort()));
		write(o, message);
	}

	// GameServer @ 148
	public void logchat(Object o, ServerThread from, ServerThread to, String m) {
		String message = String.format("Sending From [%12s:%d] To [%12s%d] : %s\n", from.getSocketAddress(),
				from.getID(), to.getSocketAddress(), to.getID(), m);
		writeToClientFile(o, message);
	}

	// StartServer @ 38
	// GameServer @ 186
	public void close() {
		try {
			clientLogger.close();
			serverLogger.close();
		} catch (Exception e) {
			System.out.printf("Error while closing log file: %s\n", e.getMessage());
		}
	}

	private String format(Object o, String message) {
		return String.format("[Time: %23s] Class: %-12s: %s", getDateTime(), o.getClass().getSimpleName(), message);
	}

	private String getDateTime() {
		Format formatter = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss:SSS");
		return formatter.format((new Date()).getTime());
	}

}
