package utils;

/*
 * when logger level is set, for example 
 * Logger.setLevel(Level.STDOUT), 
 * the logger will log messages from that level
 * and above ( HIGH, MEDIUM, LOW) 
 */
public enum Level {
	HIGH, MEDIUM, LOW, STDOUT, OFF;
}
