package utils;

public class Config {
	public static final int MIN_CLIENTS = 2;
	public static final int MAX_CLIENTS = 5;
	public static final int DEFAULT_PORT = 5950;
	public static final boolean PRINT_STACK_TRACE = false;
	public static final String DEFAULT_HOST = "localhost";
	public static final String[] DEFAULT_NAMES = { "Alpha", "Bravo", "Charlie", "Delta", "Echo" };

}
