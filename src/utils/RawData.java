package utils;

import java.util.HashMap;

public class RawData {

	// array for accessing or setting card colour value
	public String[] colors = { "purple", "red", "blue", "yellow", "green" };
	// Creating a list of action cards to use for initialization in the deck
	// adding all cards now but we can decide on which cards to use and shorten
	// the list accordingly
	public String[] actionCards = { "dodge", "disgrace", "retreat", "riposte", "outmanuever", "counter-charge",
			"charge" };
	public int[] actionTypes = { 700, 701, 702, 703, 706, 707, 708 };

	// first digit of serial is position in colours, followed by two digit
	// representing quantity

	// Types would be the serial numbers
	public int[] purpleTypes = { 0, 4, 8, 12 };
	public int[] purpleValues = { 3, 4, 5, 7 };

	public int[] redTypes = { 100, 106, 112 };
	public int[] redValues = { 3, 4, 5 };

	public int[] blueTypes = { 200, 204, 208, 212 };
	public int[] blueValues = { 2, 3, 4, 5 };

	public int[] yellowTypes = { 300, 304, 312 };
	public int[] yellowValues = { 2, 3, 4 };

	public int[] greenTypes = { 400 };
	public int[] greenValues = { 1 };

	public int[] supporterTypes = { 500, 504, 512 };
	public int[] supporterValues = { 6, 2, 3 };

	public static final HashMap<Integer, Integer> serial2Value = new HashMap<Integer, Integer>();

	public RawData() {
		String[] colors = { "purple", "red", "blue", "yellow", "green" };

		serial2Value.put(0, 3);
		serial2Value.put(4, 4);
		serial2Value.put(8, 5);
		serial2Value.put(10, 7);
		serial2Value.put(100, 3);
		serial2Value.put(106, 4);
		serial2Value.put(112, 5);
		serial2Value.put(200, 2);
		serial2Value.put(204, 4);
		serial2Value.put(208, 4);
		serial2Value.put(212, 5);
		serial2Value.put(300, 2);
		serial2Value.put(304, 3);
		serial2Value.put(312, 4);
		serial2Value.put(400, 1);
		serial2Value.put(500, 6);
		serial2Value.put(504, 2);
		serial2Value.put(512, 3);

		// setting serial codes
		serial2Value.put(666, 666);// withdraw value
		serial2Value.put(676, 676);// tournament won value
		serial2Value.put(686, 686);// game won value

		// TODO use value code for action processing?
		serial2Value.put(700, 0);
		serial2Value.put(701, 0);
		serial2Value.put(702, 0);
		serial2Value.put(703, 0);
		serial2Value.put(706, 0);
		serial2Value.put(707, 0);

	}
}
