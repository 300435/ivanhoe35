
package gui;

import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;

import gameLogic.GameData;

public class board {
	private GameData game;

	public board(GameData gameFromServer) {
		super();
		this.game = gameFromServer;
	}

	private static JLabel deckCounter;
	private static JLabel tournamentColor;
	private static JLabel highestValue;
	private static JLabel player1ValueCounter;
	private static JLabel player2ValueCounter;
	private static JLabel displayCards1;
	private static JLabel displayCards2;

	public void setDisplayCards1() {
		displayCards1.setToolTipText("");
	}

	public void setDisplayCards2() {
		displayCards1.setToolTipText("");
	}

	public void setDeckCounter(int num) {
		deckCounter.setText(num + "");
	}

	public void setDeckCounter(Color c) {
		tournamentColor.setBackground(c);
	}

	public void setHighestValue(int value) {
		tournamentColor.setText(value + "");
	}

	public void setplayer1ValueCounter(int num) {
		player1ValueCounter.setText(num + "");
	}

	public void setplayer2ValueCounter(int num) {
		player2ValueCounter.setText(num + "");
	}

	public static void addComponentsToPane(Container pane) {

		// Game variables

		JLabel deckLabel = new JLabel("DECK");
		deckCounter = new JLabel("0");
		tournamentColor = new JLabel("NONE");
		JLabel highestValueLabel = new JLabel("Highest value");
		highestValue = new JLabel("0");

		// player 1 variables
		JLabel player1 = new JLabel("Player 1");
		displayCards1 = new JLabel("Display");

		JLabel player1Value = new JLabel("Value");
		player1ValueCounter = new JLabel("0");

		// player two variables
		JLabel player2 = new JLabel("Player 2");
		displayCards2 = new JLabel("Display");

		JLabel player2Value = new JLabel("Value");
		player2ValueCounter = new JLabel("0");

		////////////////////////// test code
		ArrayList<String> ar2 = new ArrayList<String>();

		ar2.add("one");
		ar2.add("two");
		ar2.add("three");
		ar2.add("four");
		ar2.add("five");
		//////////////////////////////////////////////////
		pane.setLayout(null);

		// DECK NAME LABEL
		deckLabel.setBounds(440, 200, 80, 40);
		pane.add(deckLabel);

		// DECK COUNTER LABEL
		deckCounter.setBounds(480, 200, 80, 40);
		pane.add(deckCounter);

		// Tournament color Label
		tournamentColor.setBounds(800, 0, 50, 20);
		// tournamentColor.setOpaque(true);
		// tournamentColor.setBackground(Color.GREEN);
		pane.add(tournamentColor);

		// Highest Value Label
		highestValueLabel.setBounds(420, 0, 100, 20);
		pane.add(highestValueLabel);

		// Highest Value
		highestValue.setBounds(450, 30, 50, 20);
		pane.add(highestValue);

		// PLAYER 1 LABEL
		player1.setBounds(50, 30, 50, 20);
		player1.setToolTipText("tokens");
		pane.add(player1);

		// COLOR CARDS(player 1)//////////////////////////////////
		displayCards1.setBounds(10, 70, 100, 20);
		displayCards1.setToolTipText(ar2.toString());
		pane.add(displayCards1);

		//// value label(player 1)
		player1Value.setBounds(120, 70, 100, 20);
		pane.add(player1Value);
		player1ValueCounter.setBounds(120, 90, 20, 40);
		pane.add(player1ValueCounter);

		// PLAYER 2 LABEL
		player2.setBounds(700, 30, 50, 20);
		player2.setToolTipText("tokens");
		pane.add(player2);

		// COLOR CARDS(player 2)//////////////////////////////////
		displayCards2.setBounds(630, 70, 100, 20);
		pane.add(displayCards2);

		//// value label(player 1)
		player2Value.setBounds(740, 70, 100, 20);
		pane.add(player2Value);
		player2ValueCounter.setBounds(740, 90, 20, 40);
		pane.add(player2ValueCounter);

		// updateBoard();

	}

	public void updateBoard() {
		// player p = new player();
		highestValue.setText("" + game.getHighestTotal());// highest value
		displayCards1.setToolTipText(game.players[0].getPlayerDisplay().toString());// for
																					// board
																					// player
																					// 1
		displayCards2.setToolTipText(game.players[1].getPlayerDisplay().toString());// for
																					// board
																					// player
																					// 2
		deckCounter.setText(game.getDeckSize() + "");// for board deck remaining
														// counter
		tournamentColor.setText(game.getStartingColor());// for board tournament
															// color
		player1ValueCounter.setText(game.players[0].getPlayerTotal() + "");// for
																			// player
																			// 1
																			// value
																			// counter
		player2ValueCounter.setText(game.players[1].getPlayerTotal() + "");// for
																			// player
																			// 2
																			// value
																			// counter

		// updatePlayer(): playerHand, Opponent list, display cards

		guiPlayer.setHandList(game.players[game.getActivePlayerPosition()].getPlayerHand());
		guiPlayer.setOpponentList(game.getCurrentNumPlayers());
		guiPlayer.setDisplayList(game.players[game.getActivePlayerPosition()].getPlayerDisplay());
	}

	private static void createAndShowGUI() {

		JFrame.setDefaultLookAndFeelDecorated(true);

		JFrame frame = new JFrame("Board");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addComponentsToPane(frame.getContentPane());

		frame.pack();
		frame.setVisible(true);
		frame.setSize(900, 500);
		frame.setResizable(false);
	}

	public static void main() {

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
