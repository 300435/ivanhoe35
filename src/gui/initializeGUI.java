package gui;

import gameLogic.GameData;

public class initializeGUI {

	public initializeGUI(int numPlayers) {
		int players = numPlayers;
		int state = 3;
		GameData game = new GameData(players, state);
		guiPlayer[] guiPlayers = new guiPlayer[players];
		board b = new board(game);
		board.main();

		int a = 0;
		for (guiPlayer i : guiPlayers) {
			i = new guiPlayer(game.players[a]);
			a++;
			i.main();

		}

	}

	public static void main(String[] args) {
		// int players = 2;
		// int state = 2;
		// GameData game = new GameData(players, state);
		// guiPlayer[] guiPlayers = new guiPlayer[2];
		// board b = new board(game);
		// board.main();
		//
		// int a = 0;
		// for (guiPlayer i : guiPlayers) {
		// i = new guiPlayer(game.players[a]);
		// a++;
		// i.main();
		//
		// }

	}

}