package gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import database.Cards;
import database.Player;
import gameLogic.GameData;

@SuppressWarnings("all")
public class guiPlayer {

	private Socket socket = null;
	private static JComboBox playerHand;
	private static ArrayList<Cards> handList;

	private static JButton stage;

	private static JComboBox displayHand;
	private static ArrayList<Cards> displayList;

	private static JButton withdraw;
	private static GameData game;
	private static ArrayList<Integer> opponentsList;
	private static JComboBox opponent;
	private static Player myPlayer;

	public guiPlayer(Player p) {
		this.myPlayer = p;
		this.handList = p.getPlayerHand();

	}

	public static void setHandList(ArrayList<Cards> a) {
		handList = a;
		// playerHand.removeAll();
		playerHand.addItem(handList.toArray());

	}

	public static void setDisplayList(ArrayList<Cards> a) {
		displayList = a;
		// playerHand.removeAll();
		displayHand.addItem(displayList.toArray());

	}

	public static void setOpponentList(int players) {
		opponent.removeAll();
		for (int i = 0; i < players; i++) {
			opponentsList.add(i);
		}

		opponent.addItem(opponentsList.toArray());

	}

	public static JComboBox returnPlayerHand() {
		return playerHand;
	}

	public static JButton returnStage() {
		return stage;
	}

	public static JComboBox returnDisplayHand() {
		return displayHand;
	}

	public static JButton returnWithdraw() {
		return withdraw;
	}

	public static void addComponentsToPane(Container pane) {

		handList = myPlayer.getPlayerHand();
		displayList = myPlayer.getPlayerDisplay();
		// opponentsList = ;
		pane.setLayout(null);

		/////////////////// example code used
		 ArrayList<String> ar = new ArrayList<String>();
		 ArrayList<String> ar2 = new ArrayList<String>();
		 ArrayList<String> ar3 = new ArrayList<String>();
		
		 ar2.add("one");
		 ar2.add("two");
		 ar2.add("three");
		 ar2.add("four");
		 ar2.add("five");
		/////////////////////////////////////////////////////////
		JLabel playerHandLabel = new JLabel("Player Hand");
		JLabel displayCardLabel = new JLabel("Display cards");
		// TODO ar.toArray()->myPlayer.onStage.toArray()
		JList stagedCards = new JList(myPlayer.onStage.toArray());
		JButton commit = new JButton("Commit");
		JButton removeStaged = new JButton("Remove Staged");
		JLabel opponentListLabel = new JLabel("Opponent List");
		JTextField selectedIndex = new JTextField(6);

		// Player hand label
		playerHandLabel.setBounds(20, 20, 140, 20);
		pane.add(playerHandLabel);

		// DROP DOWN MENU(PLAYER HAND)
		// TODO this is how we do it
		playerHand = new JComboBox(handList.toArray());
		playerHand.setBounds(20, 50, 140, 20);
		playerHand.setSelectedItem(null);
		pane.add(playerHand);

		// STAGED CARD DISPLAY
		stagedCards.setBounds(200, 10, 140, 200);
		pane.add(stagedCards);

		// STAGE BUTTON
		// TODO only enable if myTurn == true
		stage = new JButton(" Stage ");
		stage.setBounds(20, 100, 80, 40);
		stage.setEnabled(true);
		pane.add(stage);

		// COMMIT BUTTON
		commit.setBounds(20, 150, 80, 40);
		commit.setEnabled(false);
		pane.add(commit);

		// WITHDRAW BUTTON
		withdraw = new JButton("Withdraw");
		withdraw.setBounds(20, 200, 100, 40);
		withdraw.setEnabled(true);
		pane.add(withdraw);

		// REMOVE STAGED BUTTON
		removeStaged.setBounds(200, 220, 160, 40);
		removeStaged.setEnabled(true);
		pane.add(removeStaged);

		// DISPLAY CARD LABEL
		displayCardLabel.setBounds(20, 250, 140, 20);
		pane.add(displayCardLabel);

		// DISPLAY CARDS( IN ORDER)
		// TODO here
		displayHand = new JComboBox(displayList.toArray());
		displayHand.setBounds(20, 280, 140, 20);
		pane.add(displayHand);

		// OPPONENT LIST
		// TODO here
		opponent = new JComboBox(ar3.toArray());// TODO HERE
		opponent.setBounds(180, 320, 140, 20);
		pane.add(opponent);

		// Opponent list label
		opponentListLabel.setBounds(220, 280, 140, 20);
		pane.add(opponentListLabel);

		// Selected index textfield
		selectedIndex.setBounds(220, 350, 40, 20);
		selectedIndex.setEditable(true);
		selectedIndex.setEnabled(true);
		selectedIndex.setText("n");
		pane.add(selectedIndex);

		// ACTION LISTENER
		// METHODS......................................................
		ListSelectionListener listSelectionListener = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent listSelectionEvent) {

				if (stagedCards.isSelectionEmpty() == false) {
					removeStaged.setEnabled(true);
				}
				if (stagedCards.isSelectionEmpty() == true) {
					removeStaged.setEnabled(false);
				}

			}
		};
		stagedCards.addListSelectionListener(listSelectionListener);

		stage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// if( selected card is allowed to be staged)
				commit.setEnabled(true);
				////
				System.out.println("SELECTED INDEX:" + selectedIndex.getText());

				if (playerHand.getSelectedIndex() > -1) {
					// TODO here
					ar.add(playerHand.getSelectedItem().toString());
					// TODO here
					stagedCards.setListData(ar.toArray());
					System.out.println("in here");
					playerHand.removeItemAt(playerHand.getSelectedIndex());
				}

			}
		});

		playerHand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stagedCards.clearSelection();
			}
		});

		commit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// validate move function should be called here?
				//
				// game.players[game.getActivePlayerPosition()].setNextSlot(0);//
				// cyka
				// game.players[get].withdraw() // blyat
				stagedCards.clearSelection();

			}
		});

		withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.players[game.getActivePlayerPosition()].withdraw();
				stage.setEnabled(false);
				commit.setEnabled(false);
				withdraw.setEnabled(false);
				removeStaged.setEnabled(false);
				stagedCards.clearSelection();
				// TODO here
				ar.clear();
				// TODO here
				stagedCards.setListData(ar.toArray());
				displayHand.removeAll();
				displayList.clear();
			}
		});

		removeStaged.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// ar.remove(stagedCards.getSelectedValue());
				// playerHand.addItem(stagedCards.getSelectedValue());
				// TODO here
				for (int i = 0; i < ar.size(); i++) {
					// TODO here
					playerHand.addItem(ar.get(i));
				}
				// TODO here
				ar.clear();
				// TODO here
				stagedCards.setListData(ar.toArray());
				stagedCards.clearSelection();
				commit.setEnabled(false);
			}
		});

		displayHand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				stagedCards.clearSelection();
			}
		});

		////////////////////////////////////////////////////////////////////////

	}

	public void updatePlayer() {

	}

	private static void createAndShowGUI() {

		JFrame.setDefaultLookAndFeelDecorated(true);

		JFrame frame = new JFrame("Player");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addComponentsToPane(frame.getContentPane());

		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 600);
		frame.setResizable(false);
	}

	public void main() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}
}
