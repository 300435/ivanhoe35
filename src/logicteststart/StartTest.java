package logicteststart;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import gameLogic.GameData;

@SuppressWarnings("all")
public class StartTest {

	private static final Logger logg = Logger.getLogger(StartTest.class.getName());

	GameData data;

	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: Starting conditions test");
	}

	@Before
	public void setUp() throws IOException {
		System.out.println("@Before() Testing");
		logg.info("\n");
		logg.info("Starting testing in: " + this.getClass().getName());
		logg.setLevel(Level.ALL);

		data = new GameData();
	}

	@After
	public void tearDown() {
		System.out.println("@After() testing");
		logg.info("Finishing testing in: " + this.getClass().getName());

		data = null;
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: Finished testing");
	}

	@Test
	public void testStartingTournamentColor() {
		logg.info("At: testStartingTournamentColor");

		String startingColor = data.getStartingColor();
		if (startingColor.isEmpty()) {
			logg.info("FAILED: NO COLOR WAS CHOSEN");
			fail();
		}

		if (data.getStartingColor().equalsIgnoreCase("purple")) {
			if (data.getLastTournamentColor() == "purple") {
				logg.info("FAILED:PURPLE CANNOT BE CHOSEN CONSECUTIVELY");
				fail();
			}
		}

		logg.info("PASSED: COLOR CHOSEN WAS ACCEPTED");

	}

	@Test
	public void testTournamentStartingplayer() {
		logg.info("At: testTournamentStartingplayer");

		if (data.getTournamentNumber() == 0) {
			logg.info("FAILED: NO TOURNAMENT WAS STARTED");
			fail();
		}

		if (data.getTournamentNumber() == 1) {
			if ((data.getpurpleTokenDrawnPostion() + 1) != data.getStartingPlayerPosition()) {
				logg.info("FAILED: CORRECT PLAYER IS NOT STARTING THE TOURNAMENT");
				fail();
			}
		}

		if (data.getTournamentNumber() > 1) {
			if (data.getLastTournamentWinnerPosition() != data.getStartingPlayerPosition()) {
				logg.info("FAILED: CORRECT PLAYER IS NOT STARTING THE TOURNAMENT");
				fail();
			}
		}

		logg.info("PASSED: CORRECT PLAYER HAS STARTED THE TOURNAMENT");
	}

}
