package playingcardstest;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import gameLogic.GameData;

public class CardsPlayedTest {

	private static final Logger log = Logger.getLogger(CardsPlayedTest.class.getName());

	GameData data;

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: CardsPlayedTest");

	}

	/**
	 * This is processed/initialized before each test is conducted
	 * 
	 * @throws IOException
	 */
	@Before
	public void setUp() throws IOException {
		System.out.println("@Before(): CardsPlayedTest");

		data = new GameData();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): CardsPlayedTest");

		data = null;
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: CardsPlayedTest");
	}

	@Test
	public void testAllowedPlayForCards() {
		log.info("\n");
		log.info("@: testMissingCards");

		int playerPosition = data.getActivePlayerPosition();

		if (data.isStartingTurn() == true) {

			if ((data.HandContainsColorCards(playerPosition) == false)
					&& (data.HandContainsSupporterCards(playerPosition) == false)) {
				log.info("FAILED: STARTING PLAYER(FIRST TURN) CANNOT START, PASSED TO NEXT PLAYER");
				fail();
			}

			if ((data.HandContainsColorCards(playerPosition) == true)
					&& (data.HandContainsSupporterCards(playerPosition) == true)) {
				if (data.StagedCardsValidity() == false) {
					log.info("FAILED: STARTING PLAYER DID NOT PLAY THE REQUIRED SEQUENCE AND TYPE OF CARDS");
					fail();
				}

			}

			if ((data.HandContainsColorCards(playerPosition) == false)
					&& (data.HandContainsSupporterCards(playerPosition) == true)) {
				if (data.StagedCardsValidity() == false) {
					log.info("FAILED: STARTING PLAYER DID NOT PLAY THE REQUIRED SEQUENCE AND TYPE OF CARDS");
					fail();
				}

			}

		}

		if (data.isStartingTurn() != true) {

			if ((data.HandContainsColorCards(playerPosition) == true)
					|| (data.HandContainsSupporterCards(playerPosition) == true)) {
				if (data.StagedCardsValidity() == false) {
					log.info("FAILED: STARTING PLAYER DID NOT PLAY THE REQUIRED SEQUENCE AND OR TYPE OF CARDS");
					fail();
				}
			}

		}

		log.info("PASSED: THE PLAYER HAS PLAYED VALID CARDS SO FAR");

	}

}