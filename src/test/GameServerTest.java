package test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import network.GameServer;
import utils.Config;
import utils.Logit;

public class GameServerTest {
	GameServer gameServer;
	private Logger logger = Logit.getInstance().getLogger(this);
	// Logger logger = Logger.getLogger(TestappServer.class.getName());

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: TestGameServer");

	}

	/**
	 * This is processed/initialized before each test is conducted
	 * 
	 * @throws IOException
	 */
	@Before
	public void setUp() throws IOException {
		System.out.println("@Before(): TestGameServer");
		gameServer = new GameServer(Config.DEFAULT_PORT, Config.MAX_CLIENTS);
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): TestGameServer");
		gameServer.shutdown();
		gameServer = null;
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: TestGameServer");
	}

	@Test
	public void testInitializedState() {
		try {
			logger.debug("testInitializedState ---> testing expected port against ServerSocket.LocalPort");
			assertEquals(Config.DEFAULT_PORT, gameServer.getMainPort());
			logger.debug("testInitializedState passed");
		} catch (AssertionError ae) {
			logger.debug("testInitializedState failed");
		}

	}

	// @Test
	// public void testWaitingState() {
	// // test serverSocket as it should accept only numPlayers threads
	// // force more accepts than serverSocket has been instructed to
	// // accommodate
	// try {
	// logger.debug("testWaitingState--->verify ServerSocket accept limit");
	// for (int i = 1; i < Config.MAX_CLIENTS + 1; i++) {
	// try {
	// gameServer.serverSocket.accept();
	// } catch (IOException ioe) {
	// System.out.println("gamer server full");
	// }
	//
	// }
	//
	// assertEquals(Config.MAX_CLIENTS, GameServer.getClientCount());
	// logger.debug("testWaitingState passed");
	// } catch (AssertionError ae) {
	// logger.debug("testWaitingState failed");
	// }
	// }
	/*
	 * @Test public void testReady2BeginState() { // test initGame(numPlayers)
	 * sent to RulesEngine fail(); }
	 * 
	 * @Test public void testBeginningState() { // test acceptable Status
	 * returned from RE with valid isActive // instructions // test isActive
	 * gets card from deck fail(); }
	 * 
	 * @Test public void testStartTournamentState() { // waiting for only
	 * isActive to make move(not withdraw) // & simultaneously select tournament
	 * fail(); }
	 * 
	 * @Test public void testMoveMadeState() { // test handling of isActive's
	 * raw move and send for validation // fail(); }
	 * 
	 * @Test public void testValidMoveMadeState() { // test handling of valid
	 * indicator from RE by advancing isActive by one // fail();
	 * 
	 * }
	 * 
	 * @Test public void testNextMoveState() { // wait for isActive's move to
	 * either progress Tournament total or // withdraw // fail(); }
	 * 
	 * @Test public void testTournamentCompleteState() { // receive tournament
	 * won status from RE // progress isActive // fail(); }
	 * 
	 * @Test public void testGameCompleteState() { // receive game complete
	 * status from RE // send game summary to all clients // fail(); }
	 */
}
