package test;

import static org.junit.Assert.*;
import org.junit.*;

import database.SimpleCards;
import utils.Logit;

@SuppressWarnings("all")
public class SimpleCardsTest {
	
	//private Logger log = Logit.getInstance().getLogger(this);
	
		@Test
		public void TestCtor()
		{
			int v = 4;
			String c = "purple";
			
			SimpleCards sc = new SimpleCards(v, c);
			
			assertEquals(v, sc.getValue());
			assertEquals(c, sc.getColor());
			
//			if( (v != sc.getValue()) && (c != sc.getColor()) )
//			{
////				log.info("Testing constructor failed");
//				fail();
//			}
//			
////			log.info("Test passed");

		}
		
}
