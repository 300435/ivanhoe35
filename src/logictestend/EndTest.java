package logictestend;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import gameLogic.GameData;

@SuppressWarnings("all")
public class EndTest {

	private static final Logger logg2 = Logger.getLogger(EndTest.class.getName());

	GameData data;

	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: Ending conditions test");
	}

	@Before
	public void setUp() throws IOException {
		System.out.println("@Before() Testing");
		logg2.info("\n");
		logg2.info("Starting testing in: " + this.getClass().getName());
		logg2.setLevel(Level.ALL);

		data = new GameData();

	}

	@After
	public void tearDown() {
		System.out.println("@After() testing");
		logg2.info("Finishing testing in: " + this.getClass().getName());

		data = null;
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: Finished testing");
	}

	@Test
	public void testEndingTournament() {
		logg2.info("At: testEndingTournament");

		if (data.getCurrentNumPlayers() == 0) {
			logg2.info("FAILED: TOURNAMENT HAS NOT STARTED");
			fail();
		}

		if (data.getCurrentNumPlayers() > 1) {
			logg2.info("FAILED TOURNAMENT IS ONGOING");
			fail();
		}

		logg2.info("PASSED:TOURNAMENT HAS ENDED");

	}

	@Test
	public void testEndingGame() {
		logg2.info("At: testEndingGame");

		if ((data.getTotalPlayerCount() <= 3) && (data.getTotalPlayerCount() > 1)) {
			if (data.getUniqueTokenNumber() != 5) {
				logg2.info("FAILED: GAME HAS NOT ENDED");
				fail();
			}
		}

		if ((data.getTotalPlayerCount() <= 5) && (data.getTotalPlayerCount() >= 4)) {
			if (data.getUniqueTokenNumber() != 4) {
				logg2.info("FAILED: GAME HAS NOT ENDED");
				fail();
			}
		}

		logg2.info("PASSED: GAME HAS ENDED WITH A WINNER");

	}

}
