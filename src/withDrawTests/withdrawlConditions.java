package withDrawTests;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import gameLogic.GameData;
import logicteststart.StartTest;

public class withdrawlConditions {
	private static final Logger log = Logger.getLogger(StartTest.class.getName());

	GameData data;

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: withdrawlConditions");

	}

	/**
	 * This is processed/initialized before each test is conducted
	 * 
	 * @throws IOException
	 */
	@Before
	public void setUp() throws IOException {
		System.out.println("@Before(): withdrawlConditions");

		data = new GameData();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): withdrawlConditions");

		data = null;

	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: withdrawlConditions");
	}

	// test if the player has withdrawn and all his display cards have been
	// discarded
	@Test
	public void testRegularWithdrawing() {
		log.info("\n");
		log.info("@: testRegularWithdrawing");
		int playerPositionNum = 1;

		if (data.gethasPlayerWithdrawn(playerPositionNum) == false) {
			log.info("FAILED: PLAYER HAS NOT WITHDRAWN SUCCESSFULLY");
			fail();
		}

		if (data.getNumCardsInPlay(playerPositionNum) > 0) {
			log.info("FAILED: PLAYER HAS NOT WITHDRAWN SUCCESSFULLY");
			fail();
		}

		log.info("PASSED: PLAYER HAS WITHDRAWN SUCCESSFULLY");

	}

	@Test
	public void specialWithdrawal() {
		log.info("\n");
		log.info("@: specialWithdrawal");

		int position = data.getActivePlayerPosition();

		if (data.gethasPlayerWithdrawn(position) == false) {
			log.info("FAILED: PLAYER HAS NOT WITHDRAWN SUCCESSFULLY");
			fail();
		}

		if (data.getActivePlayerPosition() == position + 1) {
			log.info("FAILED: CORRECT PLAYER IS NOT CONTINUING");
			fail();
		}

		if (data.displayContainsMaiden(position) == true) {
			String tokenRemoved = data.getTokenRemovedFromPlayer();
			if (data.containsToken(position, tokenRemoved) == false) {
				log.info("FAILED:PLAYER DID NOT WITHDRAW SUCCESSFULLY, TOKEN WAS NOT REMOVED");
				fail();
			}
		}

		log.info("PASSED: PLAYER HAS WITHDRAWN SUCCESSFULLY AND CORRECT PLAYER IS CONTINUING");

	}

}
