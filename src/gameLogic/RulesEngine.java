package gameLogic;

import database.Cards;
import database.Player;
import utils.RawData;

@SuppressWarnings("all")
public class RulesEngine {
	RawData data;
	int startTournyInt;
	GameData game;
	Player whoPlayed;
	boolean validMove;

	public RulesEngine(GameData yolo) {
		super();
		this.game = yolo;
	}

	public boolean validateMove(int[] move, int playerNumber, int sentServerState) {
		whoPlayed = game.players[playerNumber];
		
		if(game.TestMove(move, playerNumber, sentServerState) == false) {
			validMove = false;
			return false;
		}
		// Checking if the move made is the first move of the tournament
		if (game.tournamentColour == "" || game.tournamentColour == null) {
			int newTotal = 0;
			// store actor
			if (sentServerState != 5) {// START TOURNAMENT
				validMove = false;
				return false;
			}

			for (int i = 0; i < move.length; i++) {
				if ((move[i] / 100) > 4)
					continue;
				startTournyInt = move[i] / 100;
			}
			// if it exists, Store tournament colour in last tournament colour
			if(game.lastTournamentColour != null || game.lastTournamentColour != "")
				game.lastTournamentColour = game.tournamentColour;
			game.tournamentColour = data.colors[startTournyInt];
			// consecutive purple typecheck
			if (game.lastTournamentColour == game.tournamentColour && game.tournamentColour == "purple") {
				validMove = false;
				return false;
			}
			// loop through move list, summing values
			for (int i = 0; i < move.length; i++) {
				newTotal += RawData.serial2Value.get(move[i]);
			}

			// set appropriate playerTotal to newTotal
			whoPlayed.setPlayerTotal(newTotal);
			game.nextPlayersTurn();
			validMove = true;
			whoPlayed = null;
			return true;

		}

		if(game.tournamentColour != null) {
			boolean didActionHappen = false;
			// ensure state is past tournament selection
			if (sentServerState != 6) {// MOVE_MADE
				validMove = false;
				return false;
			}
			// loop through move list, summing values
			int newTotal = 0;
			int oldTotal = game.players[playerNumber].getPlayerTotal();
			for (int i : move) {
				if (i / 100 == 7)
					didActionHappen = actionCards(i, whoPlayed);
				newTotal += RawData.serial2Value.get(i);
			}
			
			newTotal += oldTotal;

			if (didActionHappen) {
				int lastPlayerIndex;
				if (playerNumber > 0)
					lastPlayerIndex = playerNumber - 1;
				else
					lastPlayerIndex = game.players.length - 1;

				game.setHighestTotal(game.players[lastPlayerIndex].updatePlayerTotal());
			}

			if(newTotal > game.getHighestTotal()) {
				game.setHighestTotal(newTotal);

				// set appropriate playerTotal to newTotal + oldTotal
				whoPlayed.setPlayerTotal(whoPlayed.updatePlayerTotal());
				whoPlayed.commit();
				whoPlayed = null;
				validMove = true;
				return true;
			}
			
		}
		return false;
	}
	
	
	public boolean actionCards(int sNum, Player currentPlayer) {
		//game.deck.discard(sNum);
		switch (sNum) {
		case 700:
			return dodge(currentPlayer);

		case 701:
			return disgrace();

		case 702:
			return retreat(currentPlayer);

		case 703:
			return riposte(currentPlayer);

		case 706:
			return outmaneuver(currentPlayer);

		case 707:
			return counterCharge();

		case 708:
			return charge();

		default:
			return false;

		}
	}
	
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public boolean dodge(Player currentPlayer) {
		int p = currentPlayer.getMyTargetPlayer();
		for (Player p2 : game.players) {
			if (game.players[p] == p2) {
				if (p2.getDisplaySize() > 1) {
					currentPlayer.addToHand(p2.removeCardFromDisplay(currentPlayer.getMyTargetCard()));
					p2.updatePlayerTotal();
					return true;
				} else
					return false;
			}
		}
		return false;
	}
	
	public boolean disgrace() {
		for (Player p : game.players) {
			for (int i = 0; i < p.getDisplaySize(); i++)
				if (p.getCardFromDisplay(i).getsNum() / 100 == 5) {
					game.deck.discard(p.removeCardFromDisplay(i));
					p.updatePlayerTotal();
				}
			return true;
		}
		return false;
	}
	
	public boolean retreat(Player currentPlayer) {
		// get the index of the card
		int i = currentPlayer.getMyTargetCard();
		if (currentPlayer.getDisplaySize() != 1) {
			currentPlayer.addToHand(currentPlayer.removeCardFromDisplay(i));
			currentPlayer.updatePlayerTotal();
			return true;
		}
		return false;
	}
	
	public boolean riposte(Player currentPlayer) {
		int p = currentPlayer.getMyTargetPlayer();
		for (Player p2 : game.players) {
			if (game.players[p] == p2) {
				if (p2.getDisplaySize() > 1) {
					currentPlayer.addToHand(p2.removeCardFromDisplay(p2.getDisplaySize() - 1));
					p2.updatePlayerTotal();
					return true;
				} else
					return false;
			}
		}
		return false;
	}
	


	public boolean outmaneuver(Player currentPlayer) {
		for (Player p : game.players) {
			if(p != currentPlayer) {
				game.deck.discard(p.removeCardFromDisplay(p.getDisplaySize() - 1));
				p.updatePlayerTotal();
			}
		}
		return true;
	}

	

	public boolean counterCharge() {
		int highest = 0;
		for (Player p : game.players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() > highest)
					highest = p.getCardFromDisplay(i).getValue();
			}
		}
		for (Player p : game.players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() == highest)
					game.deck.discard(p.removeCardFromDisplay(i));
			}
			p.updatePlayerTotal();
			return true;
		}
		return false;
	}

	public boolean charge() {
		int lowest = 8;
		for (Player p : game.players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() < lowest)
					lowest = p.getCardFromDisplay(i).getValue();
			}
		}
		for (Player p : game.players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() == lowest)
					game.deck.discard(p.removeCardFromDisplay(i));
			}
			p.updatePlayerTotal();
			return true;
		}
		return false;
	}

	

}
