package gameLogic;

import java.util.Arrays;
import java.util.Random;

import database.Cards;
import database.Deck;
import database.Player;
import gui.guiPlayer;
import utils.RawData;

/*
 * auto deal card when turn
 * 
 */
public class GameData {

	////////////////////////////////////////////////////// added by gagan
	private int selectedIndexForActionCard;
	private boolean isStartingTurn;
	private int highestCardValue;// throughout all displays
	private int lowestCardValue;// throughout all displays
	private String lastActionCardPlayed;// used to store the last action card
	// played in
	// a game and for testing purposes after an
	// action card has been processed;
	private boolean displayCardAmount;// used when testing if the display size
	// of one
	// player is at least one after an action card
	// has been processed;
	private boolean displayCardAmountAll;// used when testing if the display
	// sizes of
	// all players is at least one after an
	// action card has been processed;
	private int targetedPlayerPos;
	private String tokenRemoved;// stores the token info when a token is removed
	// from
	// any player because of the maiden rule
	private int activePlayerPos;
	private int currentNumPlayers;// players in play at the moment
	private int totalPlayerCount;
	private int highestUniqueTokenNum;// highest number of unique tokens across
	// all
	// players
	private int lastTournamentWinnerPos;
	private int startingPlayerPos;
	private int purpleTokenDrawnPos;
	private int tournamentNumber;
	private int highestTotal;
	private boolean commitedCardsContainsActionCard;
	private Cards lastTargetedCard;
	///////////////////////////////

	int startingPlayers;
	Deck deck;
	public Player[] players;
	public guiPlayer[] guiPlayers;

	int whoseUpNow;
	Random random;
	String lastTournamentColour;
	String tournamentColour;
	RulesEngine rulesEngine;
	private boolean winTournament;
	private boolean winGame;
	RawData data;


	public GameData(int startingPlayers, int sentServerState) {
		super();
		this.startingPlayers = startingPlayers;
		this.players = new Player[startingPlayers];
		this.rulesEngine = new RulesEngine(this);
		for (int i = 0; i < startingPlayers; i++) {
			this.players[i] = new Player();

		}

		deck = new Deck();
		deck.deal(players);
		rulesEngine = new RulesEngine(this);
		// System.out.println(this);
		random = new Random();
		whoseUpNow = random.nextInt(players.length);
		players[whoseUpNow].setMyTurn(true);

	}

	public GameData() {
		// TODO Auto-generated constructor stub
	}

	public RulesEngine getRulesEngine() {
		return rulesEngine;
	}

	public void setRulesEngine(RulesEngine rulesEngine) {
		this.rulesEngine = rulesEngine;
	}

	@Override
	public String toString() {
		return "GameData [startingPlayers=" + startingPlayers + ", deck=" + deck + ", players="
				+ Arrays.toString(players) + ", random=" + random + ", lastTournamentColour=" + lastTournamentColour
				+ ", tournamentColour=" + tournamentColour + "]";
	}

	public void nextPlayersTurn() {
		players[whoseUpNow].setMyTurn(false);
		whoseUpNow = (players[whoseUpNow] == players[players.length]) ? 0 : whoseUpNow++;
		players[whoseUpNow].setMyTurn(true);
	}

	//////////////////// added by
	//////////////////// gagan///////////////////////////////////////////////////////////
	public String getLastTournamentColor() {
		return lastTournamentColour;
	}

	public void setLastTournamentColor(String c) {
		lastTournamentColour = c;

	}

	public int getpurpleTokenDrawnPostion() {
		return purpleTokenDrawnPos;
	}

	public void setpurpleTokenDrawnPostion(int p) {
		purpleTokenDrawnPos = p;
	}

	public int getStartingPlayerPosition() {
		return startingPlayerPos;
	}

	public void setStartingPlayerPosition(int p) {
		startingPlayerPos = p;
	}

	public int getLastTournamentWinnerPosition() {
		return lastTournamentWinnerPos;
	}

	public void setLastTournamentWinnerPosition(int p) {
		lastTournamentWinnerPos = p;
	}

	public String getStartingColor() {
		return tournamentColour;
	}

	public void setStartingColor(String c) {
		tournamentColour = c;
	}

	public int getCurrentNumPlayers() {
		return currentNumPlayers;
	}

	public void setCurrentNumPlayers(int num) {
		currentNumPlayers = num;
	}

	public int getTotalPlayerCount() {

		return totalPlayerCount;
	}

	public int getUniqueTokenNumber() {

		return highestUniqueTokenNum;
	}

	public void setUniqueTokenNumber(int num) {
		highestUniqueTokenNum = num;
	}

	public boolean gethasPlayerWithdrawn(int playerPosition) {
		return players[playerPosition].isWithdrawn();
	}

	public int getNumCardsInPlay(int playerPosition) {
		return players[playerPosition].getDisplaySize();
	}

	public int getActivePlayerPosition() {
		return activePlayerPos;
	}

	public void setActivePlayerPosition(int p) {
		activePlayerPos = p;
	}

	public boolean displayContainsMaiden(int position) {
		for (int i = 0; i < players[position].getDisplaySize(); i++) {
			if (players[position].getPlayerDisplay().get(i).getsNum() == 500)
				return true;
		}
		return false;
	}

	public String getTokenRemovedFromPlayer() {
		return tokenRemoved;
	}

	public void setTokenRemovedFromPlayer(String t) {
		tokenRemoved = t;
	}

	// TOKEN CHECK
	public boolean containsToken(int position, String tokenRemoved) {

		return players[position].containsToken(tokenRemoved);
	}

	public boolean isStartingTurn() {

		return isStartingTurn;
	}

	// COLOR CARDS CHECK
	public boolean HandContainsColorCards(int playerPosition) {
		for (Player player : players) {
			for (Cards card : player.getPlayerHand())
				if (card.getsNum() / 100 < 5) {
					return true;
				}
		}
		return false;
	}

	public boolean HandContainsSupporterCards(int playerPosition) {
		for (Player player : players) {
			for (Cards card : player.getPlayerHand())
				if (card.getsNum() / 100 == 5) {
					return true;
				}
		}
		return false;
	}

	public boolean StagedCardsValidity() {
		// check if first card staged is a color card, if that color card
		// matches current tournament color,
		// and if other color cards staged are of the tournament color as well.
		// staged cards are automatically valid if they contain only supporter
		// cards(unless its first turn in a tournament, in which case the player
		// has to stage a color card first if they have any)
		// staged cards are checked as they are added( can be checked when none
		// are staged, in which case automatically valid)
		return false;
	}

	public boolean getDisplayCardAmountAll() {
		// display card amount minus the cards that will be removed
		// cannot be less than 1(return false if less)
		displayCardAmountAll = true;
		for (Player p : players) {
			if (p.getDisplaySize() < 1) {
				displayCardAmountAll = false;
				return displayCardAmount;
			}
		}
		return displayCardAmountAll;
	}

	// display card amount minus the cards that will be removed
	// cannot be less than 1(return false if less)
	public boolean getDisplayCardAmount(int playerNum) {

		// display card amount minus the cards that will be removed
		// cannot be less than 1(return false if less)

		displayCardAmountAll = true;
		if (players[playerNum].getDisplaySize() < 1) {
			displayCardAmountAll = false;
			return displayCardAmount;
		}

		return displayCardAmount;
	}

	public boolean commitedCardsContainsActionCard(int playerNum) {
		// here playerNum may not be used since this method can be called right
		// when the server receives the committed cards
		// from the player, but i put it in here anyways.
		return false;
	}

	public String getActionCardFromCommit(int playerNum) {
		// again here playerNum may not be used since this method can be called
		// right when the server receives the committed cards
		// from the player, but i put it in here anyways.
		String card = lastActionCardPlayed;
		return card;
	}

	// returns selected index for action card effect
	public int getSelectedIndexForActionCard(int activePlayer) {

		return players[activePlayer].getMyTargetPlayer();
	}

	// TODO says takes the index and targeted player?
	// retreat action card
	public boolean handContains(int serialNum, int playerNum) {
		// takes the index and the targeted player for the card and
		// checks if the card pertaining to the index in the display of the
		// playerNum has been
		// taken back into the hand.this is mainly used for retreat action card
		int dsize = players[playerNum].getDisplaySize();
		int hsize = players[playerNum].getHandSize();
		serialNum = players[playerNum].removeCardFromDisplay(dsize - 1).getsNum();
		if (players[playerNum].getPlayerHand().get(hsize - 1).getsNum() == serialNum)
			return true;

		return false;
	}

	public int getTargetedPlayer() {
		return targetedPlayerPos;
	}

	public void setTargetedPlayer(int targetedPlayer) {
		targetedPlayerPos = targetedPlayer;
	}

	public int getLastDisplaySize(int playerNum) {

		return players[playerNum].getLastDisplaySize();
	}

	public void setLastDisplaySize(int playerNum, int size) {
		players[playerNum].setLastDisplaySize(size);
	}

	public int getCurrentDisplaySize(int playerNum) {

		return players[playerNum].getDisplaySize();
	}

	
	public boolean getDisplayContainsSupporterCards() {

		for (Player p : players) {
			for (int i = 1; i < p.getDisplaySize(); i++) {
				if (p.getPlayerDisplay().get(i).getsNum() / 100 == 5)
					return true;
			}
		}

		// if any of the players have a display of 2 or more and still contain
		// supporter cards, then return false.
		// the idea is that if players had any other cards with their supporter
		// cards, then their display should not
		// have any supporter cards left, but if the players had ONLY supporter
		// cards in their display, then the earliest played
		// supporter card will remain in display as per game rules( but this
		// method will still return true).

		return false;
	}

	public boolean cardAddedByAction(int activePlayer) {
		// this method will basically check whether the card that was removed
		// from an opponents display
		// was added to the active players display.
		int dsize = players[activePlayer].getDisplaySize();
		if (players[activePlayer].getPlayerDisplay().get(dsize - 1) == this.getLastTargetedCard())
			return true;

		return false;
	}

	// for outmaneuver
	public boolean getLastDisplaySizeOpponenets(int activePlayer) {
		// checks through the last display size of all opponents of active
		// player and
		// compares them with current display sizes. if current display sizes
		// are smaller than last display sizes
		// than the outmaneuver card has removed the cards from opponents
		// displays

		for (Player p : players) {
			if (p != players[activePlayer]) {
				if (p.getLastDisplaySize() == p.getDisplaySize())
					return false;
				else if (p.getLastDisplaySize() - p.getDisplaySize() == 1)
					return true;
			}
		}
		return true;
	}

	public boolean getLastDisplaySizeAll() {
		// checks through the last display size of all players and
		// compares them with their current display sizes. if current display
		// size is smaller than
		// the last display sizes than the charge or counter-charge card has
		// removed the cards from all players displays

		return true;
	}

	public int getHighestCardValue() {

		return highestCardValue;
	}

	public void setHighestCardValue(int value) {

	}

	public int getLowestCardValue() {

		return lowestCardValue;
	}

	public void setLowestCardValue(int value) {

	}

	public boolean cardsRemovedByAction(int cardValue) {
		// this method will use an array of card serials and check if all the
		// cards removed from all players have the same
		// value as the cardValue(lowest value or highest value) throughout all
		// displays, if so the method returns true, otherwise false
		return false;
	}

	public int getHighestTotal() {
		return highestTotal;
	}

	public void setHighestTotal(int highestTotal) {
		this.highestTotal = highestTotal;
	}

	public int getDeckSize() {
		// TODO Auto-generated method stub
		return deck.getDeck().size();
	}

	public int getTournamentNumber() {

		return tournamentNumber;
	}

	public Cards getLastTargetedCard() {
		return lastTargetedCard;
	}

	public void setLastTargetedCard(Cards lastTargetedCard) {
		this.lastTargetedCard = lastTargetedCard;
	}

	public boolean isCommitedCardsContainsActionCard() {
		return commitedCardsContainsActionCard;
	}

	public void setCommitedCardsContainsActionCard(boolean commitedCardsContainsActionCard) {
		this.commitedCardsContainsActionCard = commitedCardsContainsActionCard;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public boolean TestMove(int[] move, int playerPosition, int serverState) {
		//If it is the starting turn(first move of the tournament)
		if(tournamentColour == null) {
			int firstCard = move[0];
			if(firstCard >= 700)
				return false;
			
			if(HandContainsColorCards(playerPosition) && HandContainsSupporterCards(playerPosition)) {
				if(firstCard/100 > 4)
					return false;
			}
			
			if(!HandContainsColorCards(playerPosition) && HandContainsSupporterCards(playerPosition)) {
				if(firstCard/100 != 5)
					return false;
			}
			
		}
		
		if(tournamentColour != null) {
			int lastPlayer = 0;
			if(playerPosition == 0)
				lastPlayer = players.length -1;
			else if(playerPosition > 0)
				lastPlayer = playerPosition -1;
			
			int lastPlayerTotal = players[lastPlayer].getPlayerTotal();
			
			int didActionHappen = 0;
			for(int i: move) {
				if(i < 500) {
					if(data.colors[i/100] != tournamentColour)
						return false;
					break;
				}
			}
			
			int oldTotal = players[playerPosition].getPlayerTotal();
			int tempTotal = 0;
			
			for (int i : move) {
				if (i / 100 == 7) {
					didActionHappen = actionCards(i, playerPosition);
					if(i == 703) {
						tempTotal += didActionHappen;
						lastPlayerTotal -= didActionHappen;
					}
					if(i == 702)
						tempTotal += didActionHappen;
					 if(i == 701 || i == 706 || i == 700)
						lastPlayerTotal = didActionHappen;
					 if(i == 707) {
						 int tempValue = counterChargeTestCP(playerPosition);
						 lastPlayerTotal = counterChargeTestLP(playerPosition);
						 tempTotal += tempValue;
					 }
					 if(i == 708) {
						 int tempValue = chargeTestCP(playerPosition);
						 lastPlayerTotal = chargeTestLP(playerPosition);
						 tempTotal += tempValue;
					 }
				}
				else
					tempTotal += RawData.serial2Value.get(i);
			}
			
			tempTotal += oldTotal;
			
			if(tempTotal < lastPlayerTotal)
				return false;
			
		}
		
		
		return true;
	}
	
	public int actionCards(int sNum, int currentPlayer) {
		//game.deck.discard(sNum);
		
		switch (sNum) {
		case 700:
			return dodgeTest(currentPlayer);

		case 701:
			return disgraceTest(currentPlayer);

		case 702:
			return retreatTest(currentPlayer);

		case 703:
			return riposteTest(currentPlayer);

		case 706:
			return outmaneuverTest(currentPlayer);

//		case 707:
//			return counterChargeTest(currentPlayer);
//
//		case 708:
//			return chargeTest(currentPlayer);

		default:
			return 0;

		}
	}
	
	public int riposteTest(int currentPlayer) {
	
		int target = players[currentPlayer].getMyTargetPlayer();
		if((currentPlayer > 0 && target == currentPlayer - 1) || (currentPlayer == 0 && target == players.length -1)) {
			if (players[target].getDisplaySize() > 1) {
				Cards card = players[target].getCardFromDisplay(players[target].getDisplaySize() -1);
				//currentPlayer.addToHand();
				//p2.updatePlayerTotal();
				return card.getValue();
			}
		}
		return 0;
	}
	
	public int dodgeTest(int currentPlayer) {
		int target = players[currentPlayer].getMyTargetPlayer();
		if((currentPlayer > 0 && target == currentPlayer - 1) || (currentPlayer == 0 && target == players.length -1)) {
			if (players[target].getDisplaySize() > 1) {
				Cards card = players[target].getCardFromDisplay(players[currentPlayer].getMyTargetCard());
				//currentPlayer.addToHand();
				//p2.updatePlayerTotal();
				return (players[target].getPlayerTotal() - card.getValue());
			}
		}
				
		return 0;	
	}
	
	public int outmaneuverTest(int currentPlayer) {
		int lastPlayer =0;
		if(currentPlayer == 0)
			lastPlayer = players.length -1;
		else if(currentPlayer > 0)
			lastPlayer = currentPlayer -1;
		
		Cards card = players[lastPlayer].getCardFromDisplay(players[lastPlayer].getDisplaySize() - 1);
		int lastTotal = players[lastPlayer].getPlayerTotal() - card.getValue();
		return lastTotal;
	}
	
	public int disgraceTest(int currentPlayer) {
		int lastPlayer =0, lastTotal = 0;
		
		if(currentPlayer == 0)
			lastPlayer = players.length -1;
		else if(currentPlayer > 0)
			lastPlayer = currentPlayer -1;
		
		lastTotal = players[lastPlayer].getPlayerTotal();
		
		for(Cards card: players[lastPlayer].getPlayerDisplay()) {
			if(card.getsNum()/100 == 5)
				lastTotal -= card.getValue();
				
		}
		return lastTotal;
		
	}
	
	public int retreatTest(int currentPlayer) {
		// get the index of the card
		int i = players[currentPlayer].getMyTargetCard();
		if (players[currentPlayer].getDisplaySize() != 1) {
			return -players[currentPlayer].getCardFromDisplay(i).getValue();
		}
		return 0;
	}
	
	public int chargeTestCP(int currentPlayer) {
		int returningValue = 0;
		int lowest = 8;
		for (Player p : players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() < lowest)
					lowest = p.getCardFromDisplay(i).getValue();
			}
		}
		
		for (int i = 0; i < players[currentPlayer].getDisplaySize(); i++) {
			if (players[currentPlayer].getCardFromDisplay(i).getValue() == lowest)
				returningValue += lowest;
		}
		return -returningValue;
	}
	
	public int chargeTestLP(int currentPlayer) {
		int lastPlayer =0, lastTotal = 0;
		
		if(currentPlayer == 0)
			lastPlayer = players.length -1;
		else if(currentPlayer > 0)
			lastPlayer = currentPlayer -1;
		
		lastTotal = players[lastPlayer].getPlayerTotal();
		
		int lowest = 8;
		for (Player p : players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() < lowest)
					lowest = p.getCardFromDisplay(i).getValue();
			}
		}
		
		for (int i = 0; i < players[lastPlayer].getDisplaySize(); i++) {
			if (players[lastPlayer].getCardFromDisplay(i).getValue() == lowest)
				lastTotal -= lowest;
		}
		
		return lastTotal;
	}
	
	public int counterChargeTestCP(int currentPlayer) {
		int returningValue = 0;
		int highest = 0;
		for (Player p : players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() > highest)
					highest = p.getCardFromDisplay(i).getValue();
			}
		}
		
		for (int i = 0; i < players[currentPlayer].getDisplaySize(); i++) {
			if (players[currentPlayer].getCardFromDisplay(i).getValue() == highest)
				returningValue += highest;
		}
		return -returningValue;
	}
	
	public int counterChargeTestLP(int currentPlayer) {
		int lastPlayer =0, lastTotal = 0;
		
		if(currentPlayer == 0)
			lastPlayer = players.length -1;
		else if(currentPlayer > 0)
			lastPlayer = currentPlayer -1;
		
		lastTotal = players[lastPlayer].getPlayerTotal();
		
		int highest = 0;
		for (Player p : players) {
			for (int i = 0; i < p.getDisplaySize(); i++) {
				if (p.getCardFromDisplay(i).getValue() < highest)
					highest = p.getCardFromDisplay(i).getValue();
			}
		}
		
		for (int i = 0; i < players[lastPlayer].getDisplaySize(); i++) {
			if (players[lastPlayer].getCardFromDisplay(i).getValue() == highest)
				lastTotal -= highest;
		}
		
		return lastTotal;
	}
	


	
	
	
	public boolean isWinGame() {
		return winGame;
	}

	public void setWinGame(boolean winGame) {
		this.winGame = winGame;
	}
	

	public boolean isWinTournament() {
		return winTournament;
	}

	public void setWinTournament(boolean winTournament) {
		this.winTournament = winTournament;
	}

	
	public boolean checkWin() {
		int nextPlayerPos, counter = 0;
		if(activePlayerPos == players.length - 1)
			nextPlayerPos = 0;
		else
			nextPlayerPos = activePlayerPos + 1;
		for(int i = 0; i < players.length; i++) {
			if(i!=nextPlayerPos) {
				if(players[i].isWithdrawn())
					counter++;
			}
		}
		
		if(counter == players.length -1) {
			this.lastTournamentWinnerPos = nextPlayerPos;
			return true;
		}
		
		return false;
		

	}
	
	public void endTournament() {
		
		//resets everything in Player class
		deck.refreshAllDisplays(players);
		for(int i = 0; i < players.length; i++) {
			players[i].setNewTournamentConditions();
			if(i == this.lastTournamentWinnerPos) {
				players[i].wonTournament(tournamentColour, players.length);
				if(players[i].getWinTourny())
					this.setWinTournament(true);
				if(players[i].getWinGame())
					this.setWinGame(true);
			}
		}
	}
	
	public void beginNewTournament() {
		lastTournamentColour = tournamentColour;
		tournamentColour = null;
		
		for(int i = 0; i < players.length; i++)
			if(players[i].isMyTurn())
				whoseUpNow = i; 
		
		this.winGame = false;
		this.winTournament = false;
		
	}

}
